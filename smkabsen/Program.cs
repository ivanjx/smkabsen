using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using smkabsen.Services;

namespace smkabsen
{
    public class Program
    {
        public static void Main(string[] args)
        {
            // Getting port setting.
            ConfigService configService = new ConfigService();
            string url = string.Format(
                "http://localhost:{0}",
                configService.HttpPort);
            
            // Building.
            WebHost
                .CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .UseUrls(url)
                .Build()
                .Run();
        }
    }
}
