using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace smkabsen.Models 
{
    public class School 
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id 
        {
            get;
            set;
        }

        [BsonElement("npsn")]
        public string NPSN 
        {
            get;
            set;
        }

        [BsonElement("name")]
        public string Name 
        {
            get;
            set;
        }

        [BsonElement("address")]
        public string Address 
        {
            get;
            set;
        }
        
        public override bool Equals(object obj)
        {
            School s = obj as School;

            if (s == null)
            {
                return false;
            }

            return 
                Id == s.Id &&
                Name == s.Name &&
                NPSN == s.NPSN &&
                Address == s.Address;
        }
        
        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}