using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace smkabsen.Models
{
    public class AuthToken
    {
        [BsonId]
        [BsonRepresentation(BsonType.String)]
        public string Id 
        {
            get;
            set;
        }

        [BsonElement("userId")]
        [BsonRepresentation(BsonType.ObjectId)]
        public string UserId 
        {
            get;
            set;
        }

        [BsonElement("expireTime")]
        public DateTime ExpireTime 
        {
            get;
            set;
        }

        [BsonElement("ip")]
        public string Ip 
        {
            get;
            set;
        }

        [BsonIgnore]
        public bool IsValid
        {
            get
            {
                return DateTime.UtcNow < ExpireTime;
            }
        }

        public override bool Equals(object obj)
        {
            AuthToken t = obj as AuthToken;

            if (t == null)
            {
                return false;
            }

            return 
                Id == t.Id &&
                UserId == t.UserId &&
                ExpireTime == t.ExpireTime &&
                Ip == t.Ip;
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}