using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace smkabsen.Models
{
    public enum UserRole
    {
        Admin,
        Regular
    }

    public class User 
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id 
        {
            get;
            set;
        }

        [BsonElement("schoolId")]
        [BsonRepresentation(BsonType.ObjectId)]
        public string SchoolId 
        {
            get;
            set;
        }

        [BsonElement("nip")]
        public string NIP 
        {
            get;
            set;
        }

        [BsonElement("email")]
        public string Email 
        {
            get;
            set;
        }

        [BsonElement("name")]
        public string Name 
        {
            get;
            set;
        }

        [BsonElement("passwordPads")]
        public string PasswordPads 
        {
            get;
            set;
        }

        [BsonElement("passwordHash")]
        public string PasswordHash 
        {
            get;
            set;
        }

        [BsonElement("role")]
        public UserRole Role 
        {
            get;
            set;
        }

        [BsonIgnore]
        public bool IsAdmin 
        {
            get
            {
                return Role == UserRole.Admin;
            }
        }

        [BsonIgnore]
        public bool IsSuperAdmin
        {
            get
            {
                return IsAdmin && string.IsNullOrEmpty(SchoolId);
            }
        }

        public override bool Equals(object obj)
        {
            User user = obj as User;

            if (user == null)
            {
                return false;
            }

            return
                Id == user.Id &&
                SchoolId == user.SchoolId &&
                NIP == user.NIP &&
                Email == user.Email &&
                Name == user.Name &&
                PasswordPads == user.PasswordPads &&
                PasswordHash == user.PasswordHash &&
                Role == user.Role;
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}