using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using smkabsen.Exceptions;
using smkabsen.Models;
using smkabsen.Repositories;
using smkabsen.Services;

namespace smkabsen.Controllers.UserInfoController
{
    [Route("/userinfo")]
    public class UserInfoController : ControllerBase
    {
        ISchoolRepository m_schoolRepository;
        IUserService m_userService;

        public UserInfoController(
            IUserService userService,
            ISchoolRepository schoolRepository)
        {
            m_userService = userService;
            m_schoolRepository = schoolRepository;
        }

        // GET /userinfo/get
        [HttpGet("get")]
        public async Task<IActionResult> GetAsync()
        {
            // User info.
            User user = HttpContext.Items["user"] as User;
            School school = null;

            // School info.
            if (!string.IsNullOrEmpty(user.SchoolId))
            {
                school = await m_schoolRepository.GetByIdAsync(user.SchoolId);
            }

            return new SuccessActionResult(new UserDTO(user, school));
        }

        // GET /userinfo/update
        [HttpPost("update")]
        public async Task<IActionResult> UpdateAsync(
            [FromBody] UserDTO data)
        {
            // Validating.
            if (data == null)
            {
                throw new InvalidRequestBodyException();
            }

            if (string.IsNullOrEmpty(data.Email))
            {
                throw new InsufficientParameterException("email");
            }

            if (string.IsNullOrEmpty(data.Name))
            {
                throw new InsufficientParameterException("name");
            }

            if (string.IsNullOrEmpty(data.NIP))
            {
                throw new InsufficientParameterException("nip");
            }

            // Updating basic user info.
            User user = HttpContext.Items["user"] as User;
            user.Email = data.Email;
            user.Name = data.Name;
            user.NIP = data.NIP;
            await m_userService.UpdateAsync(user, user);

            // Done.
            return new SuccessActionResult();
        }

        // POST /userinfo/update/password
        [HttpPost("update/password")]
        public async Task<IActionResult> UpdatePasswordAsync(
            [FromBody] UpdatePasswordRequest data)
        {
            // Validating.
            if (data == null)
            {
                throw new InvalidRequestBodyException();
            }

            if (string.IsNullOrEmpty(data.OldPassword))
            {
                throw new InsufficientParameterException("oldPassword");
            }

            if (string.IsNullOrEmpty(data.NewPassword))
            {
                throw new InsufficientParameterException("newPassword");
            }

            // Updating password.
            User user = HttpContext.Items["user"] as User;
            await m_userService.SetPasswordAsync(
                user,
                data.OldPassword,
                data.NewPassword);

            // Done.
            return new SuccessActionResult();
        }
    }
}