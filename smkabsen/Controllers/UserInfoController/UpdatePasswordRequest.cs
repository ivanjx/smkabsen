using System;
using System.Text.Json.Serialization;

namespace smkabsen.Controllers.UserInfoController
{
    public class UpdatePasswordRequest
    {
        [JsonPropertyName("oldPassword")]
        public string OldPassword 
        {
            get;
            set;
        }

        [JsonPropertyName("newPassword")]
        public string NewPassword 
        {
            get;
            set;
        }
    }
}