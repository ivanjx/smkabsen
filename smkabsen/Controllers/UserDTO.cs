using System;
using System.Text.Json.Serialization;
using smkabsen.Models;

namespace smkabsen.Controllers
{
    public class UserDTO
    {
        [JsonPropertyName("id")]
        public string Id 
        {
            get;
            set;
        }

        [JsonPropertyName("school")]
        public SchoolDTO School 
        {
            get;
            set;
        }

        [JsonPropertyName("email")]
        public string Email 
        {
            get;
            set;
        }

        [JsonPropertyName("name")]
        public string Name 
        {
            get;
            set;
        }

        [JsonPropertyName("nip")]
        public string NIP 
        {
            get;
            set;
        }

        [JsonPropertyName("role")]
        public string Role 
        {
            get;
            set;
        }

        [JsonIgnore]
        public User Model
        {
            get
            {
                return new User()
                {
                    Id = Id,
                    SchoolId = School?.Id,
                    Email = Email,
                    Name = Name,
                    NIP = NIP,
                    Role = Role == "admin" ? 
                        UserRole.Admin : 
                        UserRole.Regular
                };
            }
        }

        public UserDTO() { }

        public UserDTO(User model, School school)
        {
            Id = model.Id;
            Email = model.Email;
            Name = model.Name;
            NIP = model.NIP;
            Role = model.IsAdmin ?
                "admin" :
                "regular";

            if (school != null)
            {
                School = new SchoolDTO(school);
            }
        }
    }
}