using System;
using System.Text.Json.Serialization;
using smkabsen.Models;

namespace smkabsen.Controllers
{
    public class SchoolDTO
    {
        [JsonPropertyName("id")]
        public string Id 
        {
            get;
            set;
        }

        [JsonPropertyName("name")]
        public string Name 
        {
            get;
            set;
        }

        [JsonPropertyName("npsn")]
        public string NPSN 
        {
            get;
            set;
        }

        [JsonPropertyName("address")]
        public string Address 
        {
            get;
            set;
        }

        [JsonIgnore]
        public School Model
        {
            get
            {
                return new School()
                {
                    Id = Id,
                    Name = Name,
                    NPSN = NPSN,
                    Address = Address
                };
            }
        }

        public SchoolDTO() { }

        public SchoolDTO(School model)
        {
            Id = model.Id;
            Name = model.Name;
            NPSN = model.NPSN;
            Address = model.Address;
        }
    }
}