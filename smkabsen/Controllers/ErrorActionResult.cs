using System;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using smkabsen.Exceptions;

namespace smkabsen.Controllers
{
    public class ErrorActionResult : IActionResult
    {
        int m_status;
        ErrorResponse m_result;

        public ErrorActionResult(Exception ex)
        {
            m_result = new ErrorResponse();
            RouteException rex = ex as RouteException;

            if (rex != null)
            {
                // Route.
                m_status = rex.StatusCode;
                m_result.Error = rex.Name;
            }
            else
            {
                // Generic.
                m_status = 500;
                m_result.Error = nameof(Exception);
            }

            m_result.Details = ex.Message;
        }

        public async Task ExecuteResultAsync(ActionContext context)
        {
            ObjectResult result = new ObjectResult(m_result);
            result.StatusCode = m_status;
            await result.ExecuteResultAsync(context);
        }
    }
}