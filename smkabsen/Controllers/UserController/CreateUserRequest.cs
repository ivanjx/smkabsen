using System;
using System.Text.Json.Serialization;
using smkabsen.Models;

namespace smkabsen.Controllers.UserController
{
    public class CreateUserRequest : UserDTO
    {
        [JsonPropertyName("password")]
        public string Password 
        {
            get;
            set;
        }
    }
}