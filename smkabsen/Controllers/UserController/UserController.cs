using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using smkabsen.Exceptions;
using smkabsen.Models;
using smkabsen.Repositories;
using smkabsen.Services;

namespace smkabsen.Controllers.UserController
{
    [Route("/user")]
    public class UserController : ControllerBase
    {
        const int LIST_LIMIT = 10;

        IUserRepository m_userRepository;
        ISchoolRepository m_schoolRepository;
        IUserService m_userService;

        public UserController(
            IUserRepository userRepository,
            ISchoolRepository schoolRepository,
            IUserService userService)
        {
            m_userRepository = userRepository;
            m_schoolRepository = schoolRepository;
            m_userService = userService;
        }

        async Task<UserDTO> ConvertDTOAsync(User user)
        {
            School school = null;

            if (!user.IsSuperAdmin)
            {
                school = await m_schoolRepository.GetByIdAsync(user.SchoolId);
            }

            return new UserDTO(user, school);
        }

        // GET /user/search
        [HttpGet("/user/search")]
        public async Task<IActionResult> SearchAsync(
            [FromQuery(Name = "schoolId")] string schoolId,
            [FromQuery(Name = "email")] string email,
            [FromQuery(Name = "skip")] int? skip,
            [FromQuery(Name = "limit")] int? limit)
        {
            // Getting user info.
            User user = HttpContext.Items["user"] as User;

            // Generating filter.
            User searchData = new User();
            searchData.Email = email;
            searchData.SchoolId = schoolId;

            if (!user.IsSuperAdmin)
            {
                // Can only list from specific school.
                searchData.SchoolId = user.SchoolId;
            }

            // Listing.
            User[] users = await m_userRepository.SearchAsync(
                searchData,
                skip ?? 0,
                limit ?? LIST_LIMIT);
            var resultTask = users
                .Select(async x => await ConvertDTOAsync(x));
            UserDTO[] result = await Task.WhenAll(resultTask);

            // Done.
            return new SuccessActionResult(result);
        }

        // POST /user/create
        [HttpGet("create")]
        public async Task<IActionResult> CreateAsync(
            [FromBody] CreateUserRequest data)
        {
            // Validating.
            User user = HttpContext.Items["user"] as User;

            if (data == null)
            {
                throw new InvalidRequestBodyException();
            }

            if (!user.IsSuperAdmin && 
                string.IsNullOrEmpty(data.School?.Id))
            {
                throw new InsufficientParameterException("schoolId");
            }

            if (string.IsNullOrEmpty(data.Email))
            {
                throw new InsufficientParameterException("email");
            }

            if (string.IsNullOrEmpty(data.Name))
            {
                throw new InsufficientParameterException("name");
            }

            if (string.IsNullOrEmpty(data.NIP))
            {
                throw new InsufficientParameterException("nip");
            }

            if (string.IsNullOrEmpty(data.Password))
            {
                throw new InsufficientParameterException("password");
            }

            // Creating user.
            User result = await m_userService.CreateAsync(
                user,
                data.Model, 
                data.Password);

            // Done.
            UserDTO dto = await ConvertDTOAsync(result);
            return new SuccessActionResult(dto);
        }

        // POST /user/update
        [HttpPost("update")]
        public async Task<IActionResult> UpdateAsync(
            [FromBody] CreateUserRequest data)
        {
            // Validating.
            User user = HttpContext.Items["user"] as User;

            if (data == null)
            {
                throw new InvalidRequestBodyException();
            }

            if (!user.IsSuperAdmin &&
                string.IsNullOrEmpty(data.School?.Id))
            {
                throw new InsufficientParameterException("schoolId");
            }
            
            if (string.IsNullOrEmpty(data.Email))
            {
                throw new InsufficientParameterException("email");
            }

            if (string.IsNullOrEmpty(data.Name))
            {
                throw new InsufficientParameterException("name");
            }

            if (string.IsNullOrEmpty(data.NIP))
            {
                throw new InsufficientParameterException("nip");
            }

            // Updating.
            await m_userService.UpdateAsync(
                user,
                data.Model);

            if (!string.IsNullOrEmpty(data.Password))
            {
                // Updating password.
                await m_userService.SetPasswordAsync(
                    data.Model,
                    user,
                    data.Password);
            }

            // Done.
            return new SuccessActionResult();
        }

        // POST /user/delete
        [HttpPost("delete")]
        public async Task<IActionResult> DeleteAsync(
            [FromBody] UserDTO data)
        {
            // Validating.
            if (data == null)
            {
                throw new InvalidRequestBodyException();
            }

            if (string.IsNullOrEmpty(data.Id))
            {
                throw new InsufficientParameterException("id");
            }

            // Deleting.
            User user = HttpContext.Items["user"] as User;
            await m_userService.DeleteAsync(user, data.Model);

            // Done.
            return new SuccessActionResult();
        }
    }
}