using System;
using System.Text.Json.Serialization;

namespace smkabsen.Controllers
{
    public class ErrorResponse
    {
        [JsonPropertyName("error")]
        public string Error
        {
            get;
            set;
        }

        [JsonPropertyName("details")]
        public string Details
        {
            get;
            set;
        }

        public ErrorResponse()
        {
            
        }

        public ErrorResponse(string error, string details)
        {
            Error = error;
            Details = details;
        }
    }
}