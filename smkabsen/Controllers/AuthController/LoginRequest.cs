using System;
using System.Text.Json.Serialization;

namespace smkabsen.Controllers.AuthController
{
    public class LoginRequest
    {
        [JsonPropertyName("email")]
        public string Email 
        {
            get;
            set;
        }

        [JsonPropertyName("password")]
        public string Password 
        {
            get;
            set;
        }
    }
}