using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using smkabsen.Models;
using smkabsen.Repositories;
using smkabsen.Services;
using smkabsen.Exceptions;

namespace smkabsen.Controllers.AuthController
{
    [Route("/auth")]
    public class AuthController : ControllerBase
    {
        ISchoolRepository m_schoolRepository;
        IAuthService m_authService;
        IAuthTokenWriterService m_authTokenWriterService;
        
        public AuthController(
            ISchoolRepository schoolRepository,
            IAuthService authService,
            IAuthTokenWriterService authTokenWriterService)
        {
            m_schoolRepository = schoolRepository;
            m_authService = authService;
            m_authTokenWriterService = authTokenWriterService;
        }

        // POST /auth/login
        [HttpPost("login")]
        public async Task<IActionResult> LoginAsync(
            [FromBody] LoginRequest data)
        {
            // Validating.
            User user = HttpContext.Items["user"] as User;

            if (user != null)
            {
                throw new AlreadyAuthenticatedException();
            }

            if (data == null)
            {
                throw new InvalidRequestBodyException();
            }

            if (string.IsNullOrEmpty(data.Email))
            {
                throw new InsufficientParameterException("email");
            }

            if (string.IsNullOrEmpty(data.Password))
            {
                throw new InsufficientParameterException("password");
            }

            // Authenticating.
            (AuthToken Token, User User) result = await m_authService.AuthenticateAsync(
                data.Email,
                data.Password,
                HttpContext.Items["ip"] as string);

            // Writing token.
            m_authTokenWriterService.WriteToken(result.Token);

            // Getting school.
            School school = null;

            if (!string.IsNullOrEmpty(result.User.SchoolId))
            {
                school = await m_schoolRepository.GetByIdAsync(result.User.SchoolId);
            }

            // Done.
            return new SuccessActionResult(new UserDTO(result.User, school));
        }

        // POST /auth/logout
        [HttpPost("logout")]
        public async Task<IActionResult> LogoutAsync()
        {
            // Getting token.
            string tokenId = HttpContext.Items["token"] as string;

            // Logging out.
            await m_authService.DeauthenticateAsync(tokenId);

            // Clearing token.
            m_authTokenWriterService.ClearToken();

            // Done.
            return new SuccessActionResult();
        }
    }
}