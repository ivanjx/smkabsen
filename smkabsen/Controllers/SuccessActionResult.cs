using System;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace smkabsen.Controllers
{
    public class SuccessActionResult : IActionResult
    {
        [JsonPropertyName("result")]
        public object Result
        {
            get;
            set;
        }

        public SuccessActionResult()
        {
            
        }

        public SuccessActionResult(object result)
        {
            Result = result;
        }

        public async Task ExecuteResultAsync(ActionContext context)
        {
            ObjectResult result = new ObjectResult(this);
            result.StatusCode = 200;
            await result.ExecuteResultAsync(context);
        }
    }
}