using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using smkabsen.Models;
using smkabsen.Repositories;
using smkabsen.Services;
using smkabsen.Exceptions;
using System.Linq;

namespace smkabsen.Controllers.SchoolController
{
    [Route("/school")]
    public class SchoolController : ControllerBase
    {
        const int LIST_LIMIT = 10;

        ISchoolRepository m_schoolRepository;
        ISchoolService m_schoolService;

        public SchoolController(
            ISchoolRepository schoolRepository,
            ISchoolService schoolService)
        {
            m_schoolRepository = schoolRepository;
            m_schoolService = schoolService;
        }

        // GET /school/get
        [HttpGet("get")]
        public async Task<IActionResult> GetAsync(
            [FromQuery(Name = "id")] string id)
        {
            // Validating.
            if (string.IsNullOrEmpty(id))
            {
                throw new InsufficientParameterException("id");
            }

            // Getting info.
            School school = await m_schoolRepository.GetByIdAsync(id);

            // Done.
            return new SuccessActionResult(new SchoolDTO(school));
        }

        // GET /school/search
        [HttpGet("search")]
        public async Task<IActionResult> SearchAsync(
            [FromQuery(Name = "npsn")] string npsn,
            [FromQuery(Name = "name")] string name,
            [FromQuery(Name = "skip")] int? skip,
            [FromQuery(Name = "limit")] int? limit)
        {
            // Searching.
            School searchData = new School()
            {
                NPSN = npsn,
                Name = name
            };
            School[] schools = await m_schoolRepository.SearchAsync(
                searchData,
                skip ?? 0,
                limit ?? LIST_LIMIT);

            // Done.
            SchoolDTO[] result = schools
                .Select(x => new SchoolDTO(x))
                .ToArray();
            return new SuccessActionResult(result);
        }

        // POST /school/create
        [HttpPost("create")]
        public async Task<IActionResult> CreateAsync(
            [FromBody] SchoolDTO data)
        {
            // Validating.
            if (data == null)
            {
                throw new InvalidRequestBodyException();
            }

            if (string.IsNullOrEmpty(data.Name))
            {
                throw new InsufficientParameterException("name");
            }

            if (string.IsNullOrEmpty(data.NPSN))
            {
                throw new InsufficientParameterException("npsn");
            }

            if (string.IsNullOrEmpty(data.Address))
            {
                throw new InsufficientParameterException("address");
            }

            // Creating.
            School school = await m_schoolService.CreateAsync(data.Model);

            // Done.
            return new SuccessActionResult(new SchoolDTO(school));
        }

        // POST /school/update
        [HttpPost("update")]
        public async Task<IActionResult> UpdateAsync(
            [FromBody] SchoolDTO data)
        {
            // Validating.
            if (data == null)
            {
                throw new InvalidRequestBodyException();
            }

            if (string.IsNullOrEmpty(data.Id))
            {
                throw new InsufficientParameterException("id");
            }

            if (string.IsNullOrEmpty(data.Name))
            {
                throw new InsufficientParameterException("name");
            }

            if (string.IsNullOrEmpty(data.NPSN))
            {
                throw new InsufficientParameterException("npsn");
            }

            if (string.IsNullOrEmpty(data.Address))
            {
                throw new InsufficientParameterException("address");
            }

            // Creating.
            await m_schoolService.UpdateAsync(data.Model);

            // Done.
            return new SuccessActionResult();
        }

        // POST /school/delete
        [HttpPost("delete")]
        public async Task<IActionResult> DeleteAsync(
            [FromBody] SchoolDTO data)
        {
            // Validating.
            if (data == null)
            {
                throw new InvalidRequestBodyException();
            }

            if (string.IsNullOrEmpty(data.Id))
            {
                throw new InsufficientParameterException("id");
            }

            // Deleting.
            await m_schoolService.DeleteAsync(data.Id);

            // Done.
            return new SuccessActionResult();
        }
    }
}