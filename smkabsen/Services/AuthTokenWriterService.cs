using System;
using Microsoft.AspNetCore.Http;
using smkabsen.Models;

namespace smkabsen.Services
{
    public interface IAuthTokenWriterService
    {
        void WriteToken(AuthToken token);
        void ClearToken();
    }

    public class AuthTokenWriterService : IAuthTokenWriterService
    {
        IHttpContextAccessor m_accessor;

        public AuthTokenWriterService(IHttpContextAccessor accessor)
        {
            m_accessor = accessor;
        }

        public void ClearToken()
        {
            m_accessor.HttpContext.Response.Cookies.Delete("token");
            m_accessor.HttpContext.Response.Headers.Remove("Auth-Key");
            m_accessor.HttpContext.Response.Headers.Remove("Auth-Key-Expire");
        }

        public void WriteToken(AuthToken token)
        {
            // Setting cookie.
            CookieOptions cookie = new CookieOptions();
            cookie.HttpOnly = true;
            cookie.Expires = token.ExpireTime;
            m_accessor.HttpContext.Response.Cookies.Append(
                "token",
                token.Id,
                cookie);
            
            // Setting header.
            m_accessor.HttpContext.Response.Headers["Auth-Key"] = token.Id;
            m_accessor.HttpContext.Response.Headers["Auth-Key-Expire"] = token.ExpireTime.ToString("o");
        }
    }

}