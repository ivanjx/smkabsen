using System;
using System.Security.Cryptography;
using System.Text;

namespace smkabsen.Services
{
    public interface ICryptoService
    {
        int GenerateRandomNumber(int min, int max);
        string GenerateRandomString(int len);
        string CalculateSHA256Hash(string input);
    }

    public class CryptoService : ICryptoService
    {
        const string RAND_STR = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        static Random m_rand = new Random();

        public string CalculateSHA256Hash(string input)
        {
            byte[] inputBuff = Encoding.UTF8.GetBytes(input);

            using (SHA256 hash = SHA256.Create())
            {
                byte[] hashBuff = hash.ComputeHash(inputBuff);
                StringBuilder sb = new StringBuilder();

                for (int i = 0; i < hashBuff.Length; ++i)
                {
                    sb.AppendFormat("{0:X2}", hashBuff[i]);
                }

                return sb.ToString();
            }
        }

        public int GenerateRandomNumber(int min, int max)
        {
            return m_rand.Next(min, max);
        }

        public string GenerateRandomString(int len)
        {
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < len; ++i)
            {
                int nextIndex = GenerateRandomNumber(0, RAND_STR.Length);
                sb.Append(RAND_STR[nextIndex]);
            }

            return sb.ToString();
        }
    }
}