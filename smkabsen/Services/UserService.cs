using System;
using System.Threading.Tasks;
using smkabsen.Exceptions;
using smkabsen.Models;
using smkabsen.Repositories;

namespace smkabsen.Services
{
    public interface IUserService 
    {
        Task<User> CreateAsync(
            User creator,
            User data, 
            string password);
        Task UpdateAsync(
            User creator,
            User data);
        Task SetPasswordAsync(
            User data, 
            string oldPassword, 
            string newPassword);
        Task SetPasswordAsync(
            User data,
            User executor,
            string newPassword);
        Task DeleteAsync(
            User executor,
            User data);
    }

    public class UserService : IUserService
    {
        IUserRepository m_userRepository;
        IUserPasswordService m_userPasswordService;

        public UserService(
            IUserRepository userRepository,
            IUserPasswordService userPasswordService)
        {
            m_userRepository = userRepository;
            m_userPasswordService = userPasswordService;
        }

        public async Task<User> CreateAsync(
            User creator,
            User data, 
            string password)
        {
            // Validating.
            if (data == null)
            {
                throw new ArgumentNullException(nameof(data));
            }

            if (string.IsNullOrEmpty(password))
            {
                throw new ArgumentNullException(nameof(password));
            }

            if (string.IsNullOrEmpty(data.Email))
            {
                throw new ArgumentException("Email is empty");
            }

            if (string.IsNullOrEmpty(data.Name))
            {
                throw new ArgumentException("Name is empty");
            }

            if (string.IsNullOrEmpty(data.NIP))
            {
                throw new ArgumentException("NIP is empty");
            }

            if (creator != null)
            {
                if (creator.IsSuperAdmin && !data.IsAdmin)
                {
                    throw new ArgumentException("Invalid role creation");
                }

                if (!data.IsSuperAdmin && string.IsNullOrEmpty(data.SchoolId))
                {
                    throw new ArgumentException("School id is empty");
                }
            }

            // Trimming.
            data.Id = null;
            data.Email = data.Email.Trim();
            data.Name = data.Name.Trim();
            data.NIP = data.NIP.Trim();

            // Generating password pads.
            data.PasswordPads = m_userPasswordService.GeneratePasswordPad();

            // Calculating password hash.
            data.PasswordHash = m_userPasswordService.HashPassword(password, data.PasswordPads);

            // Saving.
            return await m_userRepository.CreateAsync(data);
        }

        public Task SetPasswordAsync(User data, string oldPassword, string newPassword)
        {
            // Validating.
            if (data == null)
            {
                throw new ArgumentNullException(nameof(data));
            }

            if (string.IsNullOrEmpty(data.Id))
            {
                throw new ArgumentException("User id is empty");
            }

            if (string.IsNullOrEmpty(data.PasswordPads))
            {
                throw new ArgumentException("Password pads is empty");
            }

            if (string.IsNullOrEmpty(data.PasswordHash))
            {
                throw new ArgumentException("Passowrd hash is empty");
            }

            if (string.IsNullOrEmpty(oldPassword))
            {
                throw new ArgumentNullException(nameof(oldPassword));
            }

            if (string.IsNullOrEmpty(newPassword))
            {
                throw new ArgumentNullException(nameof(newPassword));
            }

            // Calculating old hash.
            string oldHash = m_userPasswordService.HashPassword(
                oldPassword,
                data.PasswordPads);
            
            // Verifying.
            if (oldHash != data.PasswordHash)
            {
                throw new InvalidPasswordException();
            }

            // Updating password.
            data.PasswordHash = m_userPasswordService.HashPassword(
                newPassword,
                data.PasswordPads);
            return m_userRepository.SetPasswordHashAsync(data);
        }

        public Task SetPasswordAsync(User data, User executor, string newPassword)
        {
            // Validating.
            if (data == null)
            {
                throw new ArgumentNullException(nameof(data));
            }

            if (string.IsNullOrEmpty(data.Id))
            {
                throw new ArgumentException("User id is empty");
            }

            if (string.IsNullOrEmpty(data.PasswordPads))
            {
                throw new ArgumentException("Password pads is empty");
            }

            if (string.IsNullOrEmpty(data.PasswordHash))
            {
                throw new ArgumentException("Passowrd hash is empty");
            }

            if (string.IsNullOrEmpty(newPassword))
            {
                throw new ArgumentNullException(nameof(newPassword));
            }

            if (!executor.IsAdmin)
            {
                throw new InsufficientPermissionException();
            }

            if (data.IsAdmin && !executor.IsSuperAdmin)
            {
                throw new InsufficientPermissionException();
            }

            // Updating password.
            data.PasswordHash = m_userPasswordService.HashPassword(
                newPassword,
                data.PasswordPads);
            return m_userRepository.SetPasswordHashAsync(data);
        }

        public Task UpdateAsync(
            User creator,
            User data)
        {
            // Validating.
            if (data == null)
            {
                throw new ArgumentNullException(nameof(data));
            }

            if (string.IsNullOrEmpty(data.Id))
            {
                throw new ArgumentException("Id is empty");
            }

            if (string.IsNullOrEmpty(data.Email))
            {
                throw new ArgumentException("Email is empty");
            }

            if (string.IsNullOrEmpty(data.Name))
            {
                throw new ArgumentException("Name is empty");
            }

            if (string.IsNullOrEmpty(data.NIP))
            {
                throw new ArgumentException("NIP is empty");
            }

            if (creator != null)
            {
                if (creator.IsSuperAdmin && !data.IsAdmin)
                {
                    throw new ArgumentException("Invalid role creation");
                }

                if (!data.IsSuperAdmin && string.IsNullOrEmpty(data.SchoolId))
                {
                    throw new ArgumentException("School id is empty");
                }
            }

            // Trimming.
            data.Email = data.Email.Trim();
            data.Name = data.Name.Trim();
            data.NIP = data.NIP.Trim();

            // Updating.
            return m_userRepository.UpdateAsync(data);
        }

        public async Task DeleteAsync(User executor, User data)
        {
            if (executor != null)
            {
                if (executor.Id == data.Id)
                {
                    throw new InvalidOperationException("Attempt to delete self");
                }

                if (!string.IsNullOrEmpty(executor.SchoolId) &&
                    executor.SchoolId != data.SchoolId)
                {
                    throw new InsufficientPermissionException();
                }
            }

            await m_userRepository.DeleteAsync(data.Id);
        }
    }
}