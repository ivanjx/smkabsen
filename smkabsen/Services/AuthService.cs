using System;
using System.Threading.Tasks;
using smkabsen.Models;
using smkabsen.Repositories;
using smkabsen.Exceptions;

namespace smkabsen.Services
{
    public interface IAuthService
    {
        Task<(AuthToken, User)> AuthenticateAsync(
            string identifier, 
            string password,
            string ip);
        Task<User> AuthenticateAsync(string tokenId);
        Task<DateTime> ExtendAsync(string tokenId, string ip);
        Task DeauthenticateAsync(string tokenId);
    }

    public class AuthService : IAuthService
    {
        const int MAX_TOKEN_AGE_DAYS = 30;

        IAuthTokenRepository m_authTokenRepository;
        IUserRepository m_userRepository;
        IUserPasswordService m_userPasswordService;
        ICryptoService m_cryptoService;

        public AuthService(
            IAuthTokenRepository authTokenRepository,
            IUserRepository userRepository,
            IUserPasswordService userPasswordService,
            ICryptoService cryptoService)
        {
            m_authTokenRepository = authTokenRepository;
            m_userRepository = userRepository;
            m_userPasswordService = userPasswordService;
            m_cryptoService = cryptoService;
        }

        public async Task<(AuthToken, User)> AuthenticateAsync(
            string identifier, 
            string password,
            string ip)
        {
            // Validating.
            if (string.IsNullOrEmpty(identifier))
            {
                throw new ArgumentNullException(nameof(identifier));
            }

            if (string.IsNullOrEmpty(password))
            {
                throw new ArgumentNullException(nameof(password));
            }

            // Getting user by email.
            User user = await m_userRepository.GetByEmailAsync(identifier);

            if (user == null)
            {
                throw new InvalidAuthenticationException();
            }

            // Checking password.
            string hash = m_userPasswordService.HashPassword(password, user.PasswordPads);

            if (hash != user.PasswordHash)
            {
                throw new InvalidAuthenticationException();
            }

            // Valid user.
            // Creating token.
            AuthToken token = new AuthToken();
            token.Id = m_cryptoService.GenerateRandomString(64);
            token.UserId = user.Id;
            token.ExpireTime = DateTime.UtcNow.AddDays(MAX_TOKEN_AGE_DAYS);
            token.Ip = ip;
            token = await m_authTokenRepository.CreateAsync(token);

            // Done.
            return (token, user);
        }

        void ValidateToken(AuthToken token)
        {
            if (token == null)
            {
                throw new InvalidAuthenticationException();
            }

            if (!token.IsValid)
            {
                throw new InvalidAuthenticationException();
            }
        }

        public async Task<User> AuthenticateAsync(string tokenId)
        {
            // Validating.
            if (string.IsNullOrEmpty(tokenId))
            {
                throw new ArgumentNullException(nameof(tokenId));
            }

            // Getting token.
            AuthToken token = await m_authTokenRepository.GetByIdAsync(tokenId);
            ValidateToken(token);

            // Getting user info.
            return await m_userRepository.GetByIdAsync(token.UserId);
        }

        public async Task DeauthenticateAsync(string tokenId)
        {
            // Validating.
            if (string.IsNullOrEmpty(tokenId))
            {
                throw new ArgumentNullException(nameof(tokenId));
            }

            AuthToken token = await m_authTokenRepository.GetByIdAsync(tokenId);

            if (token == null)
            {
                return;
            }

            // Setting token to expire.
            token.ExpireTime = DateTime.UtcNow;
            await m_authTokenRepository.UpdateAsync(token);
        }

        public async Task<DateTime> ExtendAsync(string tokenId, string ip)
        {
            // Validating.
            if (string.IsNullOrEmpty(tokenId))
            {
                throw new ArgumentNullException(nameof(tokenId));
            }

            AuthToken token = await m_authTokenRepository.GetByIdAsync(tokenId);
            ValidateToken(token);

            // Extending.
            token.ExpireTime = DateTime.UtcNow.AddDays(MAX_TOKEN_AGE_DAYS);
            token.Ip = ip;
            await m_authTokenRepository.UpdateAsync(token);

            // Done.
            return token.ExpireTime;
        }
    }
}