using System;

namespace smkabsen.Services
{
    public interface IUserPasswordService
    {
        string HashPassword(string password, string pads);
        string GeneratePasswordPad();
    }

    public class UserPasswordService : IUserPasswordService
    {
        ICryptoService m_cryptoService;

        public UserPasswordService(ICryptoService cryptoService)
        {
            m_cryptoService = cryptoService;
        }

        public string HashPassword(string password, string pads)
        {
            if (string.IsNullOrEmpty(password))
            {
                throw new ArgumentNullException(nameof(password));
            }

            if (string.IsNullOrEmpty(pads))
            {
                throw new ArgumentNullException(nameof(pads));
            }

            if (pads.Length < 64)
            {
                throw new ArgumentException("Invalid pads length");
            }

            return m_cryptoService.CalculateSHA256Hash(password + pads);
        }

        public string GeneratePasswordPad()
        {
            return m_cryptoService.GenerateRandomString(64);
        }
    }
}