using System;

namespace smkabsen.Services
{
    public interface IConfigService
    {
        int HttpPort
        {
            get;
        }

        string DatabaseConnectionString
        {
            get;
        }
    }

    public class ConfigService : IConfigService
    {
        public int HttpPort 
        {
            get
            {
                string str = Environment.GetEnvironmentVariable("SMKABSEN_PORT");
                int port;

                if (int.TryParse(str, out port))
                {
                    return port;
                }

                return 0;
            }
        }

        public string DatabaseConnectionString
        {
            get
            {
                return Environment.GetEnvironmentVariable("SMKABSEN_DB_CONN_STR");
            }
        }

        public ConfigService()
        {
            // Validating.
            if (HttpPort == 0)
            {
                throw new ArgumentException("HTTP port is not set");
            }

            if (string.IsNullOrEmpty(DatabaseConnectionString))
            {
                throw new ArgumentException("Database connection string is not set");
            }
        }
    }
}