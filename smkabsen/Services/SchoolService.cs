using System;
using System.Threading.Tasks;
using smkabsen.Models;
using smkabsen.Repositories;
using smkabsen.Exceptions;

namespace smkabsen.Services
{
    public interface ISchoolService
    {
        Task<School> CreateAsync(School data);
        Task UpdateAsync(School data);
        Task DeleteAsync(string id);
    }

    public class SchoolService : ISchoolService
    {
        ISchoolRepository m_schoolRepository;
        IUserRepository m_userRepository;

        public SchoolService(
            ISchoolRepository schoolRepository,
            IUserRepository userRepository)
        {
            m_schoolRepository = schoolRepository;
            m_userRepository = userRepository;
        }

        async Task ValidateAsync(School data)
        {
            if (data == null)
            {
                throw new ArgumentNullException(nameof(data));
            }

            if (string.IsNullOrEmpty(data.NPSN))
            {
                throw new ArgumentException("NPSN is empty");
            }

            if (string.IsNullOrEmpty(data.Name))
            {
                throw new ArgumentException("Name is empty");
            }

            if (string.IsNullOrEmpty(data.Address))
            {
                throw new ArgumentException("Address is empty");
            }

            // Checking for npsn duplicate.
            School dupe = await m_schoolRepository.GetByNPSNAsync(data.NPSN);

            if (dupe != null)
            {
                throw new ArgumentException("Duplicate NPSN found");
            }
        }

        public async Task<School> CreateAsync(School data)
        {
            // Validating.
            await ValidateAsync(data);

            // Creating.
            return await m_schoolRepository.CreateAsync(data);
        }

        public async Task UpdateAsync(School data)
        {
            // Validating.
            await ValidateAsync(data);

            if (string.IsNullOrEmpty(data.Id))
            {
                throw new ArgumentException("Id is empty");
            }

            // Updating.
            await m_schoolRepository.UpdateAsync(data);
        }

        public async Task DeleteAsync(string id)
        {
            // Deleting users of this school.
            await m_userRepository.DeleteBySchoolAsync(id);

            // Deleting school.
            await m_schoolRepository.DeleteAsync(id); 
        }
    }
}