using System;
using System.Threading.Tasks;
using System.Text.Json;
using Microsoft.AspNetCore.Http;
using System.Text.Json.Serialization;
using Microsoft.AspNetCore.Builder;
using System.Linq;
using smkabsen.Models;
using smkabsen.Repositories;
using smkabsen.Services;
using smkabsen.Exceptions;

namespace smkabsen.Middlewares
{
    public class UserAuthMiddleware
    {
        RequestDelegate m_next;

        public UserAuthMiddleware(RequestDelegate next)
        {
            m_next = next;
        }

        static string[] m_authRoutes = new string[]
            {
                "/auth/logout",
                "/userinfo"
            };

        bool IsAuthRoute(PathString path)
        {
            int count = m_authRoutes
                .Where(x => path.StartsWithSegments(x))
                .Count();
            return count > 0;
        }

        public async Task Invoke(
            HttpContext context,
            IAuthService authService)
        {
            // Getting token id.
            string tokenId = context.Request.Headers["Auth-Key"];
            
            if (string.IsNullOrEmpty(tokenId))
            {
                // Getting from cookie.
                tokenId = context.Request.Cookies["token"];
            }

            bool isAuthenticated = false;

            if (!string.IsNullOrEmpty(tokenId))
            {
                // Authenticating.
                User user = await authService.AuthenticateAsync(tokenId);
                context.Items["user"] = user;
                context.Items["token"] = tokenId;
                isAuthenticated = true;
            }

            // Validating route.
            if (!isAuthenticated &&
                IsAuthRoute(context.Request.Path))
            {
                // Not authenticated on auth route.
                throw new NotAuthenticatedException();
            }

            // Continue.
            await m_next.Invoke(context);
        }
    }

    public static class UserAuthMiddlewareExtension
    {
        public static IApplicationBuilder UseUserAuth(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<UserAuthMiddleware>();
        }
    }
}