using System;
using System.Threading.Tasks;
using System.Text.Json;
using Microsoft.AspNetCore.Http;
using System.Text.Json.Serialization;
using Microsoft.AspNetCore.Builder;
using smkabsen.Exceptions;
using smkabsen.Controllers;

namespace smkabsen.Middlewares
{
    public class ExceptionHandlerMiddleware
    {
        RequestDelegate m_next;

        public ExceptionHandlerMiddleware(RequestDelegate next)
        {
            m_next = next;
        }

        async Task HandleErrorAsync(
            HttpContext context, 
            Exception ex)
        {
            // Logging.
            Console.WriteLine("Exception caught:");
            Console.WriteLine(ex.ToString());

            // Content type.
            context.Response.ContentType = "application/json";

            // Error message.
            ErrorResponse result = new ErrorResponse();
            result.Details = ex.Message;

            // Determining exception type.
            RouteException rex = ex as RouteException;
            ArgumentException aex = ex as ArgumentException;

            if (rex != null)
            {
                // Route error.
                context.Response.StatusCode = rex.StatusCode;
                result.Error = rex.Name;
            }
            else if (aex != null)
            {
                // Argument error, includes null and out of range.
                context.Response.StatusCode = 400;
                result.Error = nameof(ArgumentException);
            }
            else
            {
                // Generic error.
                context.Response.StatusCode = 500;
                result.Error = nameof(Exception);
            }

            await context.Response.WriteAsync(JsonSerializer.Serialize(result));
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await m_next(context);
            }
            catch (Exception ex)
            {
                await HandleErrorAsync(context, ex);
            }
        }
    }

    public static class ExceptionHandlerMiddlewareExtension
    {
        public static IApplicationBuilder UseMyExceptionHandler(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<ExceptionHandlerMiddleware>();
        }
    }
}