using System;
using System.Threading.Tasks;
using System.Text.Json;
using Microsoft.AspNetCore.Http;
using System.Text.Json.Serialization;
using Microsoft.AspNetCore.Builder;
using System.Linq;
using smkabsen.Models;
using smkabsen.Repositories;
using smkabsen.Services;
using smkabsen.Exceptions;

namespace smkabsen.Middlewares
{
    public class SuperAdminAuthMiddleware
    {
        RequestDelegate m_next;

        public SuperAdminAuthMiddleware(RequestDelegate next)
        {
            m_next = next;
        }

        static string[] m_authRoutes = new string[]
            {
                "/school/search",
                "/school/create",
                "/school/update",
                "/school/delete",
            };

        bool IsAuthRoute(PathString path)
        {
            int count = m_authRoutes
                .Where(x => path.StartsWithSegments(x))
                .Count();
            return count > 0;
        }

        public async Task Invoke(HttpContext context)
        {
            bool isAuthenticated = false;
            User user = context.Items["user"] as User;

            if (user != null && user.IsSuperAdmin)
            {
                isAuthenticated = true;
            }

            // Validating route.
            if (!isAuthenticated &&
                IsAuthRoute(context.Request.Path))
            {
                // Not authenticated on auth route.
                throw new NotAuthenticatedException();
            }

            // Continue.
            await m_next.Invoke(context);
        }
    }

    public static class SuperAdminAuthMiddlewareExtension
    {
        public static IApplicationBuilder UseSuperAdminAuth(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<SuperAdminAuthMiddleware>();
        }
    }
}