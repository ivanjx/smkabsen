using System;
using System.Threading.Tasks;
using System.Text.Json;
using Microsoft.AspNetCore.Http;
using System.Text.Json.Serialization;
using Microsoft.AspNetCore.Builder;
using System.Linq;
using smkabsen.Models;
using smkabsen.Repositories;
using smkabsen.Services;
using smkabsen.Exceptions;

namespace smkabsen.Middlewares
{
    public class IpAddressReaderMiddleware
    {
        RequestDelegate m_next;

        public IpAddressReaderMiddleware(RequestDelegate next)
        {
            m_next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            context.Items["ip"] = 
                context.Request.Headers["x-forwarded-for"].ToString() ?? 
                context.Connection.RemoteIpAddress.ToString();

            // Continue.
            await m_next.Invoke(context);
        }
    }

    public static class IpAddressReaderMiddlewareExtension
    {
        public static IApplicationBuilder UseIpAddressReader(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<IpAddressReaderMiddleware>();
        }
    }
}