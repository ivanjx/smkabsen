using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using smkabsen.Models;
using smkabsen.Services;

namespace smkabsen.Middlewares
{
    public class UserAuthExtenderMiddleware
    {
        RequestDelegate m_next;

        public UserAuthExtenderMiddleware(RequestDelegate next)
        {
            m_next = next;
        }

        public async Task Invoke(
            HttpContext context,
            IAuthService authService,
            IAuthTokenWriterService tokenWriter)
        {
            if (context.Request.Path.StartsWithSegments("/user/info"))
            {
                // Only auto extend when getting user info.
                string tokenId = context.Items["token"] as string;

                if (!string.IsNullOrEmpty(tokenId))
                {
                    // Extending token.
                    DateTime expireTime = await authService.ExtendAsync(
                        tokenId,
                        context.Items["ip"] as string);

                    // Writing token.
                    AuthToken token = new AuthToken();
                    token.Id = tokenId;
                    token.ExpireTime = expireTime;
                    tokenWriter.WriteToken(token);
                }
            }

            // Continue.
            await m_next.Invoke(context);
        }
    }

    public static class UserAuthExtenderMiddlewareExtension
    {
        public static IApplicationBuilder UseUserAuthExtender(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<UserAuthExtenderMiddleware>();
        }
    }

}