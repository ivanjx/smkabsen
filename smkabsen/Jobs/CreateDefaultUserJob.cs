using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using smkabsen.Models;
using smkabsen.Repositories;
using smkabsen.Services;

namespace smkabsen.Jobs
{
    public class CreateDefaultUserJob : IHostedService
    {
        IUserRepository m_userRepository;
        IUserService m_userService;

        public CreateDefaultUserJob(
            IUserRepository userRepository,
            IUserService userService)
        {
            m_userRepository = userRepository;
            m_userService = userService;
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            try
            {
                long count = await m_userRepository.CountAsync();

                if (count == 0)
                {
                    // Creating default user.
                    Console.WriteLine("Creating default user...");
                    User user = new User();
                    user.Name = "admin";
                    user.Email = "admin@mail.com";
                    user.NIP = "admin";
                    user.Role = UserRole.Admin;
                    await m_userService.CreateAsync(null, user, "admin");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Unable to check for default user:");
                Console.WriteLine(ex.ToString());
            }
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }
    }
}