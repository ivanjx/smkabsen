using System;

namespace smkabsen.Exceptions
{
    public class RouteException : Exception
    {
        public int StatusCode
        {
            get;
            set;
        }

        public string Name
        {
            get
            {
                return this.GetType().Name;
            }
        }

        public RouteException(string message, int statusCode) : base(message)
        {
            StatusCode = statusCode;
        }
    }
}