using System;

namespace smkabsen.Exceptions 
{
    public class InsufficientPermissionException : RouteException
    {
        public InsufficientPermissionException() : base("Insufficient permission", 400)
        {

        }
    }
}