using System;

namespace smkabsen.Exceptions 
{
    public class InvalidPasswordException : RouteException
    {
        public InvalidPasswordException() : base("Invalid password", 400)
        {

        }
    }
}