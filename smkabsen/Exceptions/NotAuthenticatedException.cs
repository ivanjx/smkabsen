using System;

namespace smkabsen.Exceptions 
{
    public class NotAuthenticatedException : RouteException
    {
        public NotAuthenticatedException() : base("Not authenticated", 400)
        {

        }
    }
}