using System;

namespace smkabsen.Exceptions 
{
    public class InsufficientParameterException : RouteException
    {
        public InsufficientParameterException(string name) : 
            base("Insufficient parameter: " + name, 400)
        {

        }
    }
}