using System;

namespace smkabsen.Exceptions 
{
    public class InvalidAuthenticationException : RouteException
    {
        public InvalidAuthenticationException() : base("Invalid authentication", 400)
        {

        }
    }
}