using System;

namespace smkabsen.Exceptions 
{
    public class InvalidRequestBodyException : RouteException
    {
        public InvalidRequestBodyException() : base("Invalid request body", 400)
        {

        }
    }
}