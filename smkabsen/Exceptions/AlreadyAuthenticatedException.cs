using System;

namespace smkabsen.Exceptions 
{
    public class AlreadyAuthenticatedException : RouteException
    {
        public AlreadyAuthenticatedException() : base("Already authenticated", 400)
        {

        }
    }
}