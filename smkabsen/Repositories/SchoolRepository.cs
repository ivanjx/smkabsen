using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using smkabsen.Models;

namespace smkabsen.Repositories
{
    public interface ISchoolRepository
    {
        Task<School[]> ListAsync(School filterData, int skip, int limit);
        Task<School[]> SearchAsync(School searchData, int skip, int limit);
        Task<School> GetByIdAsync(string id);
        Task<School> GetByNPSNAsync(string npsn);
        Task<School> CreateAsync(School data);
        Task UpdateAsync(School data);
        Task DeleteAsync(string id);
    }

    public class SchoolRepository : ISchoolRepository
    {
        IMongoCollection<School> m_coll;

        public SchoolRepository(IDbConn dbConn)
        {
            m_coll = dbConn.GetCollection<School>("schools");
        }
        
        public async Task<School> CreateAsync(School data)
        {
            await m_coll.InsertOneAsync(data);
            return data;
        }

        BsonRegularExpression Regx(string str)
        {
            return new BsonRegularExpression(str, "i");
        }

        public async Task<School[]> ListAsync(School filterData, int skip, int limit)
        {
            List<FilterDefinition<School>> filters = new List<FilterDefinition<School>>();

            if (!string.IsNullOrEmpty(filterData?.NPSN))
            {
                filters.Add(Builders<School>.Filter.Regex(
                    x => x.NPSN,
                    Regx(filterData.NPSN)));
            }

            if (!string.IsNullOrEmpty(filterData?.Name))
            {
                filters.Add(Builders<School>.Filter.Regex(
                    x => x.Name,
                    Regx(filterData.Name)));
            }

            if (!string.IsNullOrEmpty(filterData?.Address))
            {
                filters.Add(Builders<School>.Filter.Regex(
                    x => x.Address,
                    Regx(filterData.Address)));
            }

            FilterDefinition<School> filter = Builders<School>.Filter.Empty;

            if (filters.Count > 0)
            {
                filter = Builders<School>.Filter.And(filters);
            }

            // Listing.
            var result = await m_coll
                .Find(filter)
                .Skip(skip)
                .Limit(limit)
                .ToListAsync();
            return result.ToArray();
        }

        public async Task<School[]> SearchAsync(School searchData, int skip, int limit)
        {
            List<FilterDefinition<School>> filters = new List<FilterDefinition<School>>();

            if (!string.IsNullOrEmpty(searchData?.NPSN))
            {
                filters.Add(Builders<School>.Filter.Regex(
                    x => x.NPSN,
                    Regx(searchData.NPSN)));
            }

            if (!string.IsNullOrEmpty(searchData?.Name))
            {
                filters.Add(Builders<School>.Filter.Regex(
                    x => x.Name,
                    Regx(searchData.Name)));
            }

            if (!string.IsNullOrEmpty(searchData?.Address))
            {
                filters.Add(Builders<School>.Filter.Regex(
                    x => x.Address,
                    Regx(searchData.Address)));
            }

            FilterDefinition<School> filter = Builders<School>.Filter.Empty;

            if (filters.Count > 0)
            {
                filter = Builders<School>.Filter.Or(filters);
            }

            // Listing.
            var result = await m_coll
                .Find(filter)
                .Skip(skip)
                .Limit(limit)
                .ToListAsync();
            return result.ToArray();
        }

        FilterDefinition<School> FilterById(string id)
        {
            return Builders<School>.Filter.Eq(x => x.Id, id);
        }

        public Task<School> GetByNPSNAsync(string npsn)
        {
            var filter = Builders<School>.Filter.Eq(x => x.NPSN, npsn);
            return m_coll
                .Find(filter)
                .Limit(1)
                .FirstOrDefaultAsync();
        }

        public Task<School> GetByIdAsync(string id)
        {
            return m_coll
                .Find(FilterById(id))
                .Limit(1)
                .FirstOrDefaultAsync();
        }

        public Task DeleteAsync(string id)
        {
            return m_coll.DeleteOneAsync(FilterById(id));
        }

        public Task UpdateAsync(School data)
        {
            UpdateDefinition<School> update = Builders<School>.Update
                .Set(x => x.NPSN, data.NPSN)
                .Set(x => x.Name, data.Name)
                .Set(x => x.Address, data.Address);
            return m_coll.UpdateOneAsync(
                FilterById(data.Id),
                update);
        }
    }
}