using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using smkabsen.Models;

namespace smkabsen.Repositories
{
    public interface IUserRepository
    {
        Task<User> CreateAsync(User data);
        Task<long> CountAsync();
        Task<User> GetByIdAsync(string id);
        Task<User> GetByEmailAsync(string email);
        Task<User[]> SearchAsync(User filterData, int skip, int limit);
        Task UpdateAsync(User data);
        Task SetPasswordHashAsync(User data);
        Task DeleteAsync(string id);
        Task DeleteBySchoolAsync(string schoolId);
    }

    public class UserRepository : IUserRepository
    {
        IMongoCollection<User> m_coll;

        public UserRepository(IDbConn dbConn)
        {
            m_coll = dbConn.GetCollection<User>("users");
        }

        public async Task<User> CreateAsync(User data)
        {
            await m_coll.InsertOneAsync(data);
            return data;
        }

        FilterDefinition<User> FilterById(string id)
        {
            return Builders<User>.Filter.Eq(x => x.Id, id);
        }

        public Task DeleteAsync(string id)
        {
            return m_coll.DeleteOneAsync(FilterById(id));
        }

        public Task<long> CountAsync()
        {
            return m_coll
                .Find(Builders<User>.Filter.Empty)
                .CountDocumentsAsync();
        }

        public Task<User> GetByIdAsync(string id)
        {
            return m_coll
                .Find(FilterById(id))
                .Limit(1)
                .FirstOrDefaultAsync();
        }

        public Task<User> GetByEmailAsync(string email)
        {
            FilterDefinition<User> filter = Builders<User>.Filter
                .Eq(x => x.Email, email);
            return m_coll
                .Find(filter)
                .Limit(1)
                .FirstOrDefaultAsync();
        }

        FilterDefinition<User> FilterBySchoolId(string id)
        {
            return Builders<User>.Filter.Eq(x => x.SchoolId, id);
        }

        public async Task<User[]> ListBySchoolAsync(string schoolId)
        {
            var result = await m_coll
                .Find(FilterBySchoolId(schoolId))
                .ToListAsync();
            return result.ToArray();
        }

        BsonRegularExpression Regx(string str)
        {
            return new BsonRegularExpression(str, "i");
        }

        public async Task<User[]> SearchAsync(
            User searchData,
            int skip,
            int limit)
        {
            List<FilterDefinition<User>> filters = new List<FilterDefinition<User>>();
            FilterDefinition<User> filterBySchool = Builders<User>.Filter.Empty;

            if (!string.IsNullOrEmpty(searchData?.SchoolId))
            {
                filterBySchool = Builders<User>.Filter.Eq(
                    x => x.SchoolId, 
                    searchData.SchoolId);
            }

            if (!string.IsNullOrEmpty(searchData?.Name))
            {
                filters.Add(Builders<User>.Filter.Regex(
                    x => x.Name,
                    Regx(searchData.Name)));
            }

            if (!string.IsNullOrEmpty(searchData?.Email))
            {
                filters.Add(Builders<User>.Filter.Regex(
                    x => x.Email,
                    Regx(searchData.Email)));
            }

            FilterDefinition<User> filter = Builders<User>.Filter.Empty;

            if (filterBySchool != Builders<User>.Filter.Empty)
            {
                filter = filterBySchool;
            }

            if (filters.Count > 0)
            {
                filter = Builders<User>.Filter.And(
                    filter,
                    Builders<User>.Filter.Or(filters));
            }

            // Listing.
            var result = await m_coll
                .Find(filter)
                .Skip(skip)
                .Limit(limit)
                .ToListAsync();
            return result.ToArray();
        }

        public Task UpdateAsync(User data)
        {
            UpdateDefinition<User> update = Builders<User>.Update
                .Set(x => x.NIP, data.NIP)
                .Set(x => x.Name, data.Name)
                .Set(x => x.Email, data.Email);
            return m_coll.UpdateOneAsync(
                FilterById(data.Id),
                update);
        }

        public Task SetPasswordHashAsync(User data)
        {
            UpdateDefinition<User> update = Builders<User>.Update
                .Set(x => x.PasswordHash, data.PasswordHash);
            return m_coll.UpdateOneAsync(
                FilterById(data.Id),
                update);
        }

        public Task DeleteBySchoolAsync(string schoolId)
        {
            return m_coll.DeleteManyAsync(FilterBySchoolId(schoolId));
        }
    }
}