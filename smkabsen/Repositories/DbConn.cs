using System;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Conventions;
using MongoDB.Driver;
using smkabsen.Services;

namespace smkabsen.Repositories
{
    public interface IDbConn
    {
        IMongoCollection<T> GetCollection<T>(string name);
    }

    public class DbConn : IDbConn
    {
        const int MAX_DB_CONN_TIMEOUT = 5000;

        IMongoDatabase m_db;
        MongoClient m_client;

        public DbConn(
            IConfigService configService)
        {
            // Getting connection string.
            string connStr = configService.DatabaseConnectionString;
            Console.WriteLine("Got db conn str: {0}", connStr);

            // Mongodb convention to ignore extra elements by default.
            var conventionPack = new ConventionPack
            {
                new IgnoreExtraElementsConvention(true)
            };
            ConventionRegistry.Register("IgnoreExtraElements", conventionPack, type => true);

            // Creating client.
            Console.WriteLine("Creating db client...");
            m_client = new MongoClient(connStr);

            // Getting database.
            Console.WriteLine("Getting database...");
            MongoUrl url = MongoUrl.Create(connStr);
            IMongoDatabase db = m_client.GetDatabase(url.DatabaseName);
            Console.WriteLine("Will connect to db: {0}", url.DatabaseName);

            // Testing connection.
            Console.WriteLine("Testing db connection...");
            Task t = db.RunCommandAsync((Command<BsonDocument>)"{ping:1}");
            int tresult = Task.WaitAny(new Task[] { t }, MAX_DB_CONN_TIMEOUT);

            if (tresult == -1)
            {
                throw new Exception("DB connection timed out");
            }

            // Done.
            m_db = db;
        }

        IMongoDatabase GetDatabase()
        {
            if (m_db == null)
            {
                throw new Exception("DB is initializing");
            }

            return m_db;
        }

        public IMongoCollection<T> GetCollection<T>(string name)
        {
            IMongoDatabase db = GetDatabase();
            return db.GetCollection<T>(name);
        }
    }
}