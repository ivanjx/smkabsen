using System;
using System.Threading.Tasks;
using MongoDB.Driver;
using smkabsen.Models;

namespace smkabsen.Repositories
{
    public interface IAuthTokenRepository
    {
        Task<AuthToken> GetByIdAsync(string id);
        Task<AuthToken> CreateAsync(AuthToken data);
        Task UpdateAsync(AuthToken data);
    }

    public class AuthTokenRepository : IAuthTokenRepository
    {
        IMongoCollection<AuthToken> m_coll;

        public AuthTokenRepository(IDbConn dbConn)
        {
            m_coll = dbConn.GetCollection<AuthToken>("authTokens");
        }

        public async Task<AuthToken> CreateAsync(AuthToken data)
        {
            await m_coll.InsertOneAsync(data);
            return data;
        }

        FilterDefinition<AuthToken> FilterById(string id)
        {
            return Builders<AuthToken>.Filter.Eq(x => x.Id, id);
        }

        public Task<AuthToken> GetByIdAsync(string id)
        {
            return m_coll
                .Find(FilterById(id))
                .Limit(1)
                .FirstOrDefaultAsync();
        }

        public Task UpdateAsync(AuthToken data)
        {
            UpdateDefinition<AuthToken> update = Builders<AuthToken>.Update
                .Set(x => x.ExpireTime, data.ExpireTime)
                .Set(x => x.Ip, data.Ip);
            return m_coll.UpdateOneAsync(
                FilterById(data.Id),
                update);
        }
    }
}