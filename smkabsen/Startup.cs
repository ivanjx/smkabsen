using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using smkabsen.Jobs;
using smkabsen.Middlewares;
using smkabsen.Repositories;
using smkabsen.Services;

namespace smkabsen
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            // Repositories.
            services.AddSingleton<IDbConn, DbConn>();
            services.AddSingleton<ISchoolRepository, SchoolRepository>();
            services.AddSingleton<IUserRepository, UserRepository>();
            services.AddSingleton<IAuthTokenRepository, AuthTokenRepository>();

            // Services.
            services.AddSingleton<ICryptoService, CryptoService>();
            services.AddSingleton<IConfigService, ConfigService>();
            services.AddSingleton<IUserPasswordService, UserPasswordService>();
            services.AddSingleton<IUserService, UserService>();
            services.AddSingleton<IAuthService, AuthService>();
            services.AddTransient<IAuthTokenWriterService, AuthTokenWriterService>();
            services.AddSingleton<ISchoolService, SchoolService>();

            // Jobs.
            services.AddHostedService<CreateDefaultUserJob>();
            
            // Controllers.
            services.AddControllers();

            // HTTP context accessor for token writer service.
            services.AddHttpContextAccessor();

            // CORS policy.
            services.AddCors(x =>
            {
                x.AddPolicy(
                    "MyPolicy",
                    builder =>
                    {
                        builder
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .SetIsOriginAllowed(_ => true)
                        .AllowCredentials();
                    });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            // CORS.
            app.UseCors("MyPolicy");

            // Ip address reader.
            app.UseIpAddressReader();

            // Exception handler.
            app.UseMyExceptionHandler();

            // User auth.
            app.UseUserAuth();

            // User auth extender.
            app.UseUserAuthExtender();

            // Super admin auth.
            app.UseSuperAdminAuth();

            // Admin auth.
            app.UseAdminAuth();

            // ASP NET boilerplates.
            app.UseRouting();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
