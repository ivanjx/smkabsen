using System;
using System.Collections.Specialized;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNetCore.Components.WebAssembly.Http;

namespace smkabsen.Web.Services
{
    public interface IApiService
    {
        Task<T> Get<T>(string path, NameValueCollection query = null);
        Task<T> Post<T>(string path, object data);
        Task Post(string path, object data);
    }

    public class ApiException : Exception
    {
        public string Name 
        {
            get;
            set;
        }

        public ApiException(string name, string message) : base(message)
        {
            Name = name;
        }
    }

    public class ApiService : IApiService
    {
        #region RESPONSES

        class Response<T>
        {
            [JsonPropertyName("result")]
            public T Result
            {
                get;
                set;
            }
        }

        class Error
        {
            [JsonPropertyName("error")]
            public string Name
            {
                get;
                set;
            }

            [JsonPropertyName("details")]
            public string Details
            {
                get;
                set;
            }
        }

        #endregion

        string API 
        {
            get
            {
                if (m_configService.IsDevelopment)
                {
                    return "http://localhost:5000";
                }
                else
                {
                    return "https://api.monitoring.smkn43jkt.sch.id";
                }
            }
        }

        HttpClient m_client;
        IConfigurationService m_configService;

        public ApiService(
            HttpClient client,
            IConfigurationService configService)
        {
            m_client = client;
            m_configService = configService;
        }

        public string ToQueryString(NameValueCollection collection)
        {
            if (collection == null || collection.Count == 0)
            {
                return string.Empty;
            }

            string[] array = collection.AllKeys
                .Select(key => 
                    string.Format("{0}={1}", 
                        HttpUtility.UrlEncode(key), 
                        HttpUtility.UrlEncode(collection.GetValues(key).FirstOrDefault())))
                .ToArray();
            return "?" + string.Join("&", array);
        }

        string GenerateUrl(string path, NameValueCollection query = null)
        {
            return API + path + ToQueryString(query);
        }

        async Task HandleErrorResponseAsync(HttpResponseMessage msg)
        {
            Error error;

            try
            {
                // Reading error content.
                error = await msg.Content.ReadFromJsonAsync<Error>();
            }
            catch (Exception ex)
            {
                // Error parsing json.
                throw new Exception(
                    string.Format(
                        "Connection error: {0}",
                        msg.StatusCode),
                    ex);
            }

            throw new ApiException(
                error.Name, 
                error.Details);
        }

        async Task<T> ParseResponse<T>(HttpResponseMessage response)
        {
            Response<T> r = await response.Content.ReadFromJsonAsync<Response<T>>();
            return r.Result;
        }

        HttpRequestMessage GenerateRequest(string path, NameValueCollection query = null)
        {
            HttpRequestMessage request = new HttpRequestMessage();
            request.Method = HttpMethod.Get;
            request.RequestUri = new Uri(GenerateUrl(path, query));
            request.SetBrowserRequestCredentials(BrowserRequestCredentials.Include);
            return request;
        }

        public async Task<T> Get<T>(string path, NameValueCollection query = null)
        {
            // Sending request.
            HttpResponseMessage response = await m_client.SendAsync(
                GenerateRequest(path, query));

            if (!response.IsSuccessStatusCode)
            {
                // Something went wrong.
                await HandleErrorResponseAsync(response);
                return default;
            }

            // Parsing response.
            return await ParseResponse<T>(response);
        }

        HttpRequestMessage GenerateRequest(string path, object data)
        {
            HttpRequestMessage request = new HttpRequestMessage();
            request.Method = HttpMethod.Post;
            request.RequestUri = new Uri(GenerateUrl(path));
            request.Content = new StringContent(
                JsonSerializer.Serialize(data),
                Encoding.UTF8,
                "application/json");
            request.SetBrowserRequestCredentials(BrowserRequestCredentials.Include);
            return request;
        }

        public async Task<T> Post<T>(string path, object data)
        {
            // Sending request.
            HttpResponseMessage response = await m_client.SendAsync(
                GenerateRequest(path, data));

            if (!response.IsSuccessStatusCode)
            {
                // Something went wrong.
                await HandleErrorResponseAsync(response);
                return default;
            }

            // Reading response.
            return await ParseResponse<T>(response);
        }

        public async Task Post(string path, object data)
        {
            // Sending request.
            HttpResponseMessage response = await m_client.SendAsync(
                GenerateRequest(path, data));

            if (!response.IsSuccessStatusCode)
            {
                // Something went wrong.
                await HandleErrorResponseAsync(response);
                return;
            }
        }
    }
}