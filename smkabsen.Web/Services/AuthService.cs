using System;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using smkabsen.Web.Models;
using smkabsen.Web.Repositories;

namespace smkabsen.Web.Services
{
    public interface IAuthService
    {
        Task<User> AuthenticateAsync(string email, string password);
        Task DeauthenticateAsync();
    }
    public class AuthService : IAuthService
    {
        class AuthRequest
        {
            [JsonPropertyName("email")]
            public string Email 
            {
                get;
                set;
            }

            [JsonPropertyName("password")]
            public string Password 
            {
                get;
                set;
            }

            public AuthRequest(string email, string password)
            {
                Email = email;
                Password = password;
            }
        }

        IApiService m_apiService;

        public AuthService(IApiService apiService)
        {
            m_apiService = apiService;
        }

        public Task<User> AuthenticateAsync(string email, string password)
        {
            // Validating.
            if (string.IsNullOrEmpty(email))
            {
                throw new ArgumentNullException(nameof(email));
            }

            if (string.IsNullOrEmpty(password))
            {
                throw new ArgumentNullException(nameof(password));
            }

            // Sending request.
            return m_apiService.Post<User>(
                "/auth/login",
                new AuthRequest(email, password));
        }

        public Task DeauthenticateAsync()
        {
            // Sending request.
            return m_apiService.Post("/auth/logout", null);
        }
    }
}