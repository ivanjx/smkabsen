using System;
using System.Timers;

namespace smkabsen.Web.Services
{
    public class ThrottledEvent
    {
        public delegate void RaisedEventHandler(object param);
        public event RaisedEventHandler Raised;

        bool m_flag;
        Timer m_timer;
        object m_param;

        public ThrottledEvent(int sampleSecs)
        {
            m_timer = new Timer();
            m_timer.Interval = TimeSpan.FromSeconds(sampleSecs).TotalMilliseconds;
            m_timer.Elapsed += M_timer_Elapsed;
            m_timer.AutoReset = false;
        }

        private void M_timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            m_flag = false;
            Raised?.Invoke(m_param);
        }

        public void Fire(object param)
        {
            if (!m_flag)
            {
                m_flag = true;
                m_param = param;
                m_timer.Start();
            }
        }
    }
}