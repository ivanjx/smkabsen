using System;
using System.Threading.Tasks;
using Microsoft.JSInterop;

namespace smkabsen.Web.Services
{
    public interface IAlertService
    {
        Task ShowAsync(string message);
        Task<bool> PromptAsync(string message);
    }

    public class AlertService : IAlertService
    {
        IJSRuntime m_js;

        public AlertService(IJSRuntime js)
        {
            m_js = js;
        }

        public async Task ShowAsync(string message)
        {
            await m_js.InvokeVoidAsync("alert", message);
        }

        public async Task<bool> PromptAsync(string message)
        {
            return await m_js.InvokeAsync<bool>("confirm", message);
        }
    }
}