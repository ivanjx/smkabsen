using System;

namespace smkabsen.Web.Services
{
    public interface IConfigurationService
    {
        bool IsDevelopment
        {
            get;
        }
    }

    public class ConfigurationService : IConfigurationService
    {
        public bool IsDevelopment 
        {
            get;
            set;
        }
    }
}