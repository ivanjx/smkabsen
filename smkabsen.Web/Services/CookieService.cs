using System;
using System.Threading.Tasks;
using Microsoft.JSInterop;

namespace smkabsen.Web.Services
{
    public interface ICookieService
    {
        Task SetCookieAsync(string name, string value, long expireSecs);
        Task<string> GetCookieAsync(string name);
    }

    public class CookieService : ICookieService
    {
        IJSRuntime m_jsrt;

        public CookieService(IJSRuntime jsrt)
        {
            m_jsrt = jsrt;
        }

        public async Task SetCookieAsync(string name, string value, long expireSecs)
        {
            await m_jsrt.InvokeAsync<string>("methods.createCookie", name, value, expireSecs);
        }

        public async Task<string> GetCookieAsync(string name)
        {
            return await m_jsrt.InvokeAsync<string>("methods.getCookie", name);
        }
    }
}