using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Threading.Tasks;
using smkabsen.Web.Models;
using smkabsen.Web.Services;

namespace smkabsen.Web.Repositories
{
    public interface ISchoolRepository
    {
        Task<School> GetAsync(string id);
        Task<School> CreateAsync(School data);
        Task<School[]> SearchAsync(
            School filter, 
            int? skip = null, 
            int? limit = null);
        Task UpdateAsync(School data);
        Task DeleteAsync(string id);
    }

    public class SchoolRepository : ISchoolRepository
    {
        IApiService m_apiService;

        public SchoolRepository(IApiService apiService)
        {
            m_apiService = apiService;
        }

        public Task<School> GetAsync(string id)
        {
            NameValueCollection query = new NameValueCollection();
            query["id"] = id;
            return m_apiService.Get<School>(
                "/school/get",
                query);
        }
        
        public Task<School> CreateAsync(School data)
        {
            return m_apiService.Post<School>(
                "/school/create",
                data);
        }

        public Task DeleteAsync(string id)
        {
            School data = new School();
            data.Id = id;
            return m_apiService.Post(
                "/school/delete",
                data);
        }

        public Task<School[]> SearchAsync(
            School filter, 
            int? skip = null, 
            int? limit = null)
        {
            NameValueCollection query = new NameValueCollection();
            query["skip"] = skip?.ToString() ?? string.Empty;
            query["limit"] = limit?.ToString() ?? string.Empty;

            if (filter != null)
            {
                query["name"] = filter.Name;
                query["npsn"] = filter.NPSN;
                query["address"] = filter.Address;
            }

            return m_apiService.Get<School[]>(
                "/school/search",
                query);
        }

        public Task UpdateAsync(School data)
        {
            return m_apiService.Post(
                "/school/update",
                data);
        }
    }
}