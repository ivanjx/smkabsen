using System;
using System.Collections.Specialized;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using smkabsen.Web.Models;
using smkabsen.Web.Services;

namespace smkabsen.Web.Repositories
{
    public interface IUserRepository
    {
        Task<User[]> SearchAsync(
            User searchData, 
            int? skip = null, 
            int? limit = null);
        Task<User> CreateAsync(User data, string password);
        Task UpdateAsync(User data, string newPassword = null);
        Task DeleteAsync(string id);
    }

    public class UserRepository : IUserRepository
    {
        class CreateUserRequest : User
        {
            [JsonPropertyName("password")]
            public string Password 
            {
                get;
                set;
            }

            public CreateUserRequest(User data, string password)
            {
                Id = data.Id;
                Email = data.Email;
                NIP = data.NIP;
                Name = data.Name;
                Role = data.Role;
                School = data.School;
                Password = password;
            }
        }

        IApiService m_apiService;

        public UserRepository(IApiService apiService)
        {
            m_apiService = apiService;
        }

        public Task<User[]> SearchAsync(
            User searchData, 
            int? skip = null, 
            int? limit = null)
        {
            NameValueCollection query = new NameValueCollection();
            query["skip"] = skip?.ToString() ?? string.Empty;
            query["limit"] = limit?.ToString() ?? string.Empty;

            if (searchData != null)
            {
                query["email"] = searchData.Email;
                query["schoolId"] = searchData.School?.Id;
            }

            return m_apiService.Get<User[]>(
                "/user/search",
                query);
        }

        public Task<User> CreateAsync(User data, string password)
        {
            return m_apiService.Post<User>(
                "/user/create",
                new CreateUserRequest(data, password));
        }

        public Task DeleteAsync(string id)
        {
            User data = new User();
            data.Id = id;
            return m_apiService.Post(
                "/user/delete",
                data);
        }

        public Task UpdateAsync(User data, string newPassword = null)
        {
            return m_apiService.Post(
                "/user/update",
                new CreateUserRequest(data, newPassword));
        }
    }
}