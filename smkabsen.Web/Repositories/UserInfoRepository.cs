using System;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using smkabsen.Web.Models;
using smkabsen.Web.Services;

namespace smkabsen.Web.Repositories
{
    public interface IUserInfoRepository
    {
        Task<User> GetAsync();
    }
    
    public class UserInfoRepository : IUserInfoRepository
    {
        IApiService m_apiService;

        public UserInfoRepository(IApiService apiService)
        {
            m_apiService = apiService;
        }

        public Task<User> GetAsync()
        {
            return m_apiService.Get<User>("/userinfo/get");
        }
    }
}