using System;
using System.Text.Json.Serialization;

namespace smkabsen.Web.Models
{
    public class School
    {
        [JsonPropertyName("id")]
        public string Id 
        {
            get;
            set;
        }

        [JsonPropertyName("npsn")]
        public string NPSN
        {
            get;
            set;
        }

        [JsonPropertyName("name")]
        public string Name 
        {
            get;
            set;
        }

        [JsonPropertyName("address")]
        public string Address 
        {
            get;
            set;
        }

        public override bool Equals(object obj)
        {
            School sch = obj as School;

            if (sch == null)
            {
                return false;
            }

            return 
                Id == sch.Id &&
                NPSN == sch.NPSN &&
                Name == sch.Name &&
                Address == sch.Address;
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}