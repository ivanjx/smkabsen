using System;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace smkabsen.Web.Models
{
    public enum UserRole
    {
        Admin,
        Regular
    }

    public class UserRoleConverter : JsonConverter<UserRole>
    {
        public override UserRole Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            string str = reader.GetString();

            switch (str)
            {
                case "admin":
                    return UserRole.Admin;

                case "regular":
                    return UserRole.Regular;
            }

            throw new Exception("Invalid user role value: " + str);
        }

        public override void Write(Utf8JsonWriter writer, UserRole value, JsonSerializerOptions options)
        {
            switch (value)
            {
                case UserRole.Admin:
                    writer.WriteStringValue("admin");
                    break;

                case UserRole.Regular:
                    writer.WriteStringValue("regular");
                    break;
            }
        }
    }

    public class User
    {
        [JsonPropertyName("id")]
        public string Id 
        {
            get;
            set;
        }

        [JsonPropertyName("school")]
        public School School 
        {
            get;
            set;
        }

        [JsonPropertyName("email")]
        public string Email 
        {
            get;
            set;
        }

        [JsonPropertyName("nip")]
        public string NIP 
        {
            get;
            set;
        }
        
        [JsonPropertyName("name")]
        public string Name 
        {
            get;
            set;
        }

        [JsonPropertyName("role")]
        [JsonConverter(typeof(UserRoleConverter))]
        public UserRole Role 
        {
            get;
            set;
        }

        [JsonIgnore]
        public bool IsAdmin
        {
            get
            {
                return Role == UserRole.Admin;
            }
        }

        [JsonIgnore]
        public bool IsSuperAdmin
        {
            get
            {
                return IsAdmin && School == null;
            }
        }

        public override bool Equals(object obj)
        {
            User user = obj as User;

            if (user == null)
            {
                return false;
            }

            bool school = 
                School?.Equals(user.School) ??
                School == user.School;

            return
                Id == user.Id &&
                Email == user.Email &&
                NIP == user.NIP &&
                Name == user.Name &&
                Role == user.Role &&
                school;
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}