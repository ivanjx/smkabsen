using System;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.WebUtilities;

namespace smkabsen.Web.Main
{
    public static class NavigationManagerExtension
    {
        public static T GetQueryStringValue<T>(
            this NavigationManager nav, 
            string key)
        {
            var uri = nav.ToAbsoluteUri(nav.Uri);

            if (QueryHelpers.ParseQuery(uri.Query).TryGetValue(key, out var valueFromQueryString))
            {
                if (typeof(T) == typeof(int) && int.TryParse(valueFromQueryString, out var valueAsInt))
                {
                    return (T)(object)valueAsInt;
                }

                if (typeof(T) == typeof(string))
                {
                    return (T)(object)valueFromQueryString.ToString();
                }

                if (typeof(T) == typeof(decimal) && decimal.TryParse(valueFromQueryString, out var valueAsDecimal))
                {
                    return (T)(object)valueAsDecimal;
                }
            }

            return default;
        }
    }
}