using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace smkabsen.Web.Main
{
    public abstract class ViewModelBase : INotifyPropertyChanged, IDataErrorInfo
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public virtual string Error => throw new NotImplementedException();

        public virtual string this[string columnName] => null;

        public bool CanValidate
        {
            get;
            set;
        }

        public void NotifyChange(string propName)
        {
            PropertyChanged?.Invoke(
                this,
                new PropertyChangedEventArgs(propName));
        }

        public void ForceValidation()
        {
            CanValidate = true;
            NotifyChange(null);
        }

        public void ResetValidation()
        {
            CanValidate = false;
            NotifyChange(null);
        }

        public bool IsError(string name)
        {
            if (!CanValidate)
            {
                return false;
            }

            string err = this[name];
            return !string.IsNullOrEmpty(err);
        }
    }
}
