using System;
using System.Windows.Input;

namespace smkabsen.Web.Main
{
    public class RelayCommand : ICommand
    {
        private readonly Predicate<object> _canExecute;
        private readonly Action<object> _execute;

        public event EventHandler CanExecuteChanged;

        public RelayCommand(Predicate<object> canExecute, Action<object> execute)
        {
            _canExecute = canExecute;
            _execute = execute;
        }

        public RelayCommand(Action<object> execute)
        {
            _canExecute = _ => true;
            _execute = execute;
        }

        public RelayCommand(Predicate<object> canExecute, Action execute)
        {
            _canExecute = canExecute;
            _execute = _ => execute.Invoke();
        }

        public RelayCommand(Action execute)
        {
            _canExecute = _ => true;
            _execute = _ => execute.Invoke();
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            if (_canExecute(parameter))
            {
                _execute(parameter);
            }
        }
    }
}