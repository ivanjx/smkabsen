using System;
using System.Threading.Tasks;
using System.Windows.Input;
using smkabsen.Web.Models;
using smkabsen.Web.Services;

namespace smkabsen.Web.Main.Login
{
    public class LoginViewModel : ViewModelBase
    {
        IAuthService m_authService;
        IAlertService m_alertService;
        AuthViewModel m_authVM;

        bool m_isLoading;
        string m_email;
        string m_password;

        public bool IsLoading 
        {
            get
            {
                return m_isLoading;
            }
            set
            {
                m_isLoading = value;
                NotifyChange(nameof(IsLoading));
            }
        }

        public string Email 
        {
            get
            {
                return m_email;
            }
            set
            {
                m_email = value;
                NotifyChange(nameof(m_email));
            }
        }

        public string Password 
        {
            get
            {
                return m_password;
            }
            set
            {
                m_password = value;
                NotifyChange(nameof(Password));
            }
        }

        public ICommand LoginCommand
        {
            get;
            private set;
        }

        public override string this[string columnName]
        {
            get
            {
                if (CanValidate)
                {
                    if (columnName == null || columnName == nameof(Email))
                    {
                        if (string.IsNullOrEmpty(Email))
                        {
                            return "Email kosong";
                        }
                        else if (!Email.Contains("@") || Email.Contains(" "))
                        {
                            return "Email tidak valid";
                        }
                    }

                    if (columnName == null || columnName == nameof(Password))
                    {
                        if (string.IsNullOrEmpty(Password))
                        {
                            return "Password kosong";
                        }
                    }
                }

                return null;
            }
        }

        public LoginViewModel(
            IAuthService authService,
            IAlertService alertService,
            AuthViewModel authVM)
        {
            m_authService = authService;
            m_alertService = alertService;
            m_authVM = authVM;

            LoginCommand = new RelayCommand(Login);
        }

        void ClearInputs()
        {
            Email = string.Empty;
            Password = string.Empty;
            ResetValidation();
        }

        async void Login()
        {
            // Force validation.
            ForceValidation();

            // Check validation.
            if (IsLoading ||
                IsError(nameof(Email)) ||
                IsError(nameof(Password)))
            {
                return;
            }

            // Valid.
            IsLoading = true;

            try
            {
                // Authenticating.
                m_authVM.User = await m_authService.AuthenticateAsync(Email, Password);
                m_authVM.IsAuthenticated = true;
                ClearInputs();
            }
            catch (Exception ex)
            {
                await m_alertService.ShowAsync("Unable to login: " + ex.Message);
            }
            finally
            {
                IsLoading = false;
            }
        }
    }
}