using System;
using System.Threading.Tasks;
using System.Windows.Input;
using smkabsen.Web.Models;
using smkabsen.Web.Repositories;
using smkabsen.Web.Services;
using smkabsen.Web.Main.SAdmin.Schools.Editor;
using System.Linq;

namespace smkabsen.Web.Main.SAdmin.Schools
{
    public class SchoolsViewModel : ViewModelBase, IDisposable
    {
        const int LIST_LIMIT = 10;

        ISchoolRepository m_schoolRepository;
        IAlertService m_alertService;
        ISchoolEditorService m_schoolEditorService;
        AuthViewModel m_authVM;

        bool m_isDisposed;
        bool m_isLoading;
        bool m_canNext;
        SchoolViewModel[] m_schools;

        public bool IsLoading 
        {
            get
            {
                return m_isLoading;
            }
            set
            {
                m_isLoading = value;
                NotifyChange(nameof(IsLoading));
            }
        }

        public bool CanNext 
        {
            get
            {
                return m_canNext;
            }
            set
            {
                m_canNext = value;
                NotifyChange(nameof(CanNext));
            }
        }

        public int Limit 
        {
            get
            {
                return LIST_LIMIT;
            }
        }

        public int Skip 
        {
            get;
            set;
        }

        public SchoolViewModel[] Schools 
        {
            get
            {
                return m_schools;
            }
            set
            {
                m_schools = value;
                NotifyChange(nameof(Schools));
            }
        }

        public ICommand CreateCommand
        {
            get;
            private set;
        }

        public SchoolsViewModel(
            ISchoolRepository schoolRepository,
            IAlertService alertService,
            ISchoolEditorService schoolEditorService,
            AuthViewModel authVM)
        {
            m_schoolRepository = schoolRepository;
            m_alertService = alertService;
            m_schoolEditorService = schoolEditorService;
            m_authVM = authVM;

            Schools = new SchoolViewModel[0];

            CreateCommand = new RelayCommand(
                _ => !IsLoading,
                Create);
        }

        SchoolViewModel ConvertToVM(School model)
        {
            SchoolViewModel vm = new SchoolViewModel(model);
            vm.EditRequested += Edit;
            vm.DeleteRequested += Delete;
            return vm;
        }

        public async void LoadSchools()
        {
            if (IsLoading)
            {
                return;
            }

            IsLoading = true;

            while (m_authVM.IsAuthenticated && !m_isDisposed)
            {
                try
                {
                    School[] schools = await m_schoolRepository.SearchAsync(null, Skip);
                    Schools = schools
                        .Select(x => ConvertToVM(x))
                        .ToArray();
                    CanNext = schools.Length == Limit;
                        
                    break;
                }
                catch (ApiException ex)
                {
                    await m_alertService.ShowAsync(
                        "Unable to load: " + ex.Message);
                        break;
                }
                catch (Exception ex)
                {
                    // Retry.
                    Console.WriteLine("Unable to load schools:");
                    Console.WriteLine(ex.ToString());
                }
            }
            
            IsLoading = false;
        }

        async void Create()
        {
            School result = await m_schoolEditorService.ShowAsync(null);

            if (result != null)
            {
                // Reload.
                LoadSchools();
            }
        }

        async void Edit(SchoolViewModel vm)
        {
            if (vm == null || IsLoading)
            {
                return;
            }

            School result = await m_schoolEditorService.ShowAsync(vm.Model);

            if (result != null)
            {
                // Reload.
                LoadSchools();
            }
        }

        async void Delete(SchoolViewModel vm)
        {
            if (vm == null || IsLoading)
            {
                return;
            }

            // Prompting.
            bool canContinue = await m_alertService.PromptAsync(string.Format(
                "Yakin ingin menghapus sekolah '{0}' ?",
                vm.Name));

            if (!canContinue)
            {
                return;
            }

            // Deleting.
            IsLoading = true;

            try
            {
                await m_schoolRepository.DeleteAsync(vm.Model.Id);
            }
            catch (Exception ex)
            {
                await m_alertService.ShowAsync("Unable to delete: " + ex.Message);
            }
            finally
            {
                IsLoading = false;
            }

            // Reload.
            LoadSchools();
        }

        public void Dispose()
        {
            m_isDisposed = true;
        }
    }
}