using System;
using System.Windows.Input;
using smkabsen.Web.Models;

namespace smkabsen.Web.Main.SAdmin.Schools
{
    public class SchoolViewModel : ViewModelBase
    {
        public delegate void EditRequestedEventHandler(SchoolViewModel sender);
        public event EditRequestedEventHandler EditRequested;

        public delegate void DeleteRequestedEventHandler(SchoolViewModel sender);
        public event DeleteRequestedEventHandler DeleteRequested;

        public School Model 
        {
            get;
            private set;
        }

        public string NPSN 
        {
            get
            {
                return Model.NPSN;
            }
        }

        public string Name 
        {
            get
            {
                return Model.Name;
            }
        }

        public string Address
        {
            get
            {
                return Model.Address;
            }
        }

        public ICommand EditCommand
        {
            get;
            private set;
        }

        public ICommand DeleteCommand
        {
            get;
            private set;
        }

        public SchoolViewModel(School model)
        {
            Model = model;
            
            EditCommand = new RelayCommand(
                () => EditRequested?.Invoke(this));
            DeleteCommand = new RelayCommand(
                () => DeleteRequested?.Invoke(this));
        }
    }
}