using System;
using System.Threading.Tasks;
using System.Windows.Input;
using smkabsen.Web.Models;
using smkabsen.Web.Repositories;
using smkabsen.Web.Services;

namespace smkabsen.Web.Main.SAdmin.Schools.Editor
{
    public class SchoolEditorViewModel : ViewModelBase
    {
        public delegate void DoneEventHandler(School school);
        public event DoneEventHandler Done;

        ISchoolRepository m_schoolRepository;
        IAlertService m_alertService;

        bool m_isLoading;
        bool m_isAdding;
        string m_npsn;
        string m_name;
        string m_address;
        School m_existingSchool;

        public bool IsLoading
        {
            get
            {
                return m_isLoading;
            }
            set
            {
                m_isLoading = value;
                NotifyChange(nameof(IsLoading));
            }
        }

        public bool IsAdding
        {
            get
            {
                return m_isAdding;
            }
            set
            {
                m_isAdding = value;
                NotifyChange(nameof(IsAdding));
            }
        }

        public string NPSN 
        {
            get
            {
                return m_npsn;
            }
            set
            {
                m_npsn = value;
                NotifyChange(nameof(NPSN));
            }
        }

        public string Name 
        {
            get
            {
                return m_name;
            }
            set
            {
                m_name = value;
                NotifyChange(nameof(Name));
            }
        }

        public string Address 
        {
            get
            {
                return m_address;
            }
            set
            {
                m_address = value;
                NotifyChange(nameof(Address));
            }
        }

        public School ExistingSchool
        {
            get
            {
                return m_existingSchool;
            }
            set
            {
                m_existingSchool = value;
                AssignExistingSchool(value);
            }
        }

        public override string this[string columnName]
        {
            get
            {
                if (CanValidate)
                {
                    if (columnName == null || columnName == nameof(NPSN))
                    {
                        if (string.IsNullOrEmpty(NPSN))
                        {
                            return "NPSN kosong";
                        }
                    }

                    if (columnName == null || columnName == nameof(Name))
                    {
                        if (string.IsNullOrEmpty(Name))
                        {
                            return "Nama sekolah kosong";
                        }
                    }

                    if (columnName == null || columnName == nameof(Address))
                    {
                        if (string.IsNullOrEmpty(Address))
                        {
                            return "Alamat sekolah kosong";
                        }
                    }
                }

                return null;
            }
        }

        public ICommand SaveCommand
        {
            get;
            private set;
        }

        public SchoolEditorViewModel(
            ISchoolRepository schoolRepository,
            IAlertService alertService)
        {
            m_schoolRepository = schoolRepository;
            m_alertService = alertService;

            SaveCommand = new RelayCommand(
                _ => !IsLoading,
                Save);
        }

        void AssignExistingSchool(School school)
        {
            if (school == null)
            {
                ClearInputs();
                IsAdding = true;
                return;
            }

            IsAdding = false;
            NPSN = school.NPSN;
            Name = school.Name;
            Address = school.Address;
        }

        void ClearInputs()
        {
            NPSN = string.Empty;
            Name = string.Empty;
            Address = string.Empty;
            ResetValidation();
        }

        public async void Save()
        {
            ForceValidation();

            if (IsLoading ||
                IsError(nameof(Name)) ||
                IsError(nameof(Address)) || 
                IsError(nameof(NPSN)))
            {
                return;
            }

            IsLoading = true;

            try
            {
                // Creating model.
                School school = new School();
                school.NPSN = NPSN;
                school.Name = Name;
                school.Address = Address;

                // Saving.
                if (IsAdding)
                {
                    school = await m_schoolRepository.CreateAsync(school);
                }
                else
                {
                    school.Id = ExistingSchool.Id;
                    await m_schoolRepository.UpdateAsync(school);
                }

                // Clearing inputs.
                ClearInputs();

                // Done.
                Done?.Invoke(school);
            }
            catch (Exception ex)
            {
                await m_alertService.ShowAsync(
                    "Unable to save: " + ex.Message);
            }
            finally
            {
                IsLoading = false;
            }
        }
    }
}