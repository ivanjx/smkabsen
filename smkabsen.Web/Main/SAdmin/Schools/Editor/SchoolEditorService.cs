using System;
using System.Threading.Tasks;
using smkabsen.Web.Models;

namespace smkabsen.Web.Main.SAdmin.Schools.Editor
{
    public interface ISchoolEditorService
    {
        Task<School> ShowAsync(School school);
    }

    public class SchoolEditorService : ISchoolEditorService
    {
        public SchoolEditor Editor
        {
            get;
            set;
        }

        public async Task<School> ShowAsync(School school)
        {
            if (Editor == null)
            {
                Console.WriteLine("Editor is NULL!!!");
                return null;
            }

            Editor.Assign(school);
            await Editor.Modal.ShowAsync();
            await Editor.Modal.WaitAsync();
            return Editor.Result;
        }
    }
}