using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Input;
using smkabsen.Web.Models;
using smkabsen.Web.Repositories;
using smkabsen.Web.Services;

namespace smkabsen.Web.Main.SAdmin.Users.Editor
{
    public class UserEditorViewModel : ViewModelBase
    {
        public delegate void DoneEventHandler(User result);
        public event DoneEventHandler Done;

        IUserRepository m_userRepository;
        ISchoolRepository m_schoolRepository;
        IAlertService m_alertService;
        AuthViewModel m_authVM;

        bool m_isLoading;
        bool m_isAdding;
        string m_email;
        string m_nip;
        string m_name;
        string m_password;
        School m_school;
        User m_existingUser;

        public bool IsLoading
        {
            get
            {
                return m_isLoading;
            }
            set
            {
                m_isLoading = value;
                NotifyChange(nameof(IsLoading));
            }
        }

        public bool IsAdding 
        {
            get
            {
                return m_isAdding;
            }
            set
            {
                m_isAdding = value;
                NotifyChange(nameof(IsAdding));
            }
        }

        public string Email 
        {
            get
            {
                return m_email;
            }
            set
            {
                m_email = value;
                NotifyChange(nameof(Email));
            }
        }

        public string NIP
        {
            get
            {
                return m_nip;
            }
            set
            {
                m_nip = value;
                NotifyChange(nameof(NIP));
            }
        }

        public string Name 
        {
            get
            {
                return m_name;
            }
            set
            {
                m_name = value;
                NotifyChange(nameof(Name));
            }
        }

        public string Password 
        {
            get
            {
                return m_password;
            }
            set
            {
                m_password = value;
                NotifyChange(nameof(Password));
            }
        }

        public School School 
        {
            get
            {
                return m_school;
            }
            set
            {
                m_school = value;
                NotifyChange(nameof(School));
            }
        }

        public User ExistingUser
        {
            get
            {
                return m_existingUser;
            }
            set
            {
                m_existingUser = value;
                AssignExistingUser(value);
            }
        }

        public override string this[string col]
        {
            get
            {
                if (CanValidate)
                {
                    if (col == null || col == nameof(Email))
                    {
                        if (string.IsNullOrEmpty(Email))
                        {
                            return "Email kosong";
                        }

                        if (!Email.Contains("@"))
                        {
                            return "Email invalid";
                        }
                    }

                    if (col == null || col == nameof(NIP))
                    {
                        if (string.IsNullOrEmpty(NIP))
                        {
                            return "NIP kosong";
                        }
                    }

                    if (col == null || col == nameof(Name))
                    {
                        if (string.IsNullOrEmpty(Name))
                        {
                            return "Nama kosong";
                        }
                    }

                    if (col == null || col == nameof(Password))
                    {
                        if (IsAdding && string.IsNullOrEmpty(Password))
                        {
                            return "Password kosong";
                        }
                    }

                    if (col == null || col == nameof(School))
                    {
                        if (School == null)
                        {
                            return "Sekolah belum dipilih";
                        }
                    }
                }

                return null;
            }
        }

        public ICommand SaveCommand
        {
            get;
            private set;
        }

        public UserEditorViewModel(
            IUserRepository userRepository,
            ISchoolRepository schoolRepository,
            IAlertService alertService,
            AuthViewModel authVM)
        {
            m_userRepository = userRepository;
            m_schoolRepository = schoolRepository;
            m_alertService = alertService;
            m_authVM = authVM;

            SaveCommand = new RelayCommand(
                _ => !IsLoading,
                Save);
        }

        void ClearInputs()
        {
            Email = string.Empty;
            NIP = string.Empty;
            Name = string.Empty;
            Password = string.Empty;
            School = null;
            ResetValidation();
        }

        void AssignExistingUser(User user)
        {
            if (user == null)
            {
                ClearInputs();
                IsAdding = true;
                return;
            }

            IsAdding = false;
            Email = user.Email;
            NIP = user.NIP;
            Name = user.Name;
            Password = string.Empty;
            School = user.School;
        }

        public async Task<IEnumerable<School>> SearchSchoolAsync(string filter)
        {
            School searchData = new School();
            searchData.NPSN = filter;
            searchData.Name = filter;
            return await m_schoolRepository.SearchAsync(searchData, limit: 5);
        }

        public async void Save()
        {
            ForceValidation();

            if (IsLoading ||
                IsError(nameof(Email)) ||
                IsError(nameof(Password)) || 
                IsError(nameof(School)))
            {
                return;
            }

            IsLoading = true;

            try
            {
                // Creating model.
                User user = new User();
                user.Email = Email;
                user.NIP = NIP;
                user.Name = Name;
                user.School = School;
                user.Role = UserRole.Admin;

                // Saving.
                if (IsAdding)
                {
                    user = await m_userRepository.CreateAsync(user, Password);
                }
                else
                {
                    user.Id = ExistingUser.Id;
                    await m_userRepository.UpdateAsync(user, Password);
                }

                // Clearing inputs.
                ClearInputs();

                // Done.
                Done?.Invoke(user);
            }
            catch (Exception ex)
            {
                await m_alertService.ShowAsync(
                    "Unable to save: " + ex.Message);
            }
            finally
            {
                IsLoading = false;
            }
        }
    }
}