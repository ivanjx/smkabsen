using System;
using System.Threading.Tasks;
using smkabsen.Web.Models;

namespace smkabsen.Web.Main.SAdmin.Users.Editor
{
    public interface IUserEditorService
    {
        Task<User> ShowAsync(User user);
    }

    public class UserEditorService : IUserEditorService
    {
        public UserEditor Editor
        {
            get;
            set;
        }

        public async Task<User> ShowAsync(User user)
        {
            if (Editor == null)
            {
                Console.WriteLine("Editor is NULL!!!");
                return null;
            }

            Editor.Assign(user);
            await Editor.Modal.ShowAsync();
            await Editor.Modal.WaitAsync();
            return Editor.Result;
        }
    }
}