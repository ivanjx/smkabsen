using System;
using System.Windows.Input;
using smkabsen.Web.Models;

namespace smkabsen.Web.Main.SAdmin.Users
{
    public class UserViewModel : ViewModelBase
    {
        public delegate void EditRequestedEventHandler(UserViewModel sender);
        public EditRequestedEventHandler EditRequested;

        public delegate void DeleteRequestedEventHandler(UserViewModel sender);
        public DeleteRequestedEventHandler DeleteRequested;

        public User Model
        {
            get;
            private set;
        }

        public string Email
        {
            get
            {
                return Model.Email;
            }
        }

        public string NIP
        {
            get
            {
                return Model.NIP;
            }
        }

        public string SchoolName
        {
            get
            {
                return Model.School?.Name;
            }
        }

        public ICommand EditCommand
        {
            get;
            private set;
        }

        public ICommand DeleteCommand
        {
            get;
            private set;
        }

        public UserViewModel(User model)
        {
            Model = model;

            EditCommand = new RelayCommand(
                _ => EditRequested?.Invoke(this));
            DeleteCommand = new RelayCommand(
                _ => DeleteRequested?.Invoke(this));
        }
    }
}