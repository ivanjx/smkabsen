using System;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using smkabsen.Web.Main.SAdmin.Users.Editor;
using smkabsen.Web.Models;
using smkabsen.Web.Repositories;
using smkabsen.Web.Services;

namespace smkabsen.Web.Main.SAdmin.Users
{
    public class UsersViewModel : ViewModelBase, IDisposable
    {
        const int LIST_LIMIT = 10;

        IUserRepository m_userRepository;
        IAlertService m_alertService;
        IUserEditorService m_editorService;
        AuthViewModel m_authVM;

        bool m_isDisposed;
        bool m_isLoading;
        bool m_canNext;
        UserViewModel[] m_users;

        public bool IsLoading
        {
            get
            {
                return m_isLoading;
            }
            set
            {
                m_isLoading = value;
                NotifyChange(nameof(IsLoading));
            }
        }

        public bool CanNext 
        {
            get
            {
                return m_canNext;
            }
            set
            {
                m_canNext = value;
                NotifyChange(nameof(CanNext));
            }
        }

        public int Skip
        {
            get;
            set;
        }

        public int Limit 
        {
            get
            {
                return LIST_LIMIT;
            }
        }

        public UserViewModel[] Users 
        {
            get
            {
                return m_users;
            }
            set
            {
                m_users = value;
                NotifyChange(nameof(Users));
            }
        }

        public ICommand CreateCommand
        {
            get;
            private set;
        }

        public UsersViewModel(
            IUserRepository userRepository,
            IAlertService alertService,
            IUserEditorService editorService,
            AuthViewModel authVM)
        {
            m_userRepository = userRepository;
            m_alertService = alertService;
            m_editorService = editorService;
            m_authVM = authVM;

            Users = new UserViewModel[0];

            CreateCommand = new RelayCommand(
                _ => !IsLoading,
                Create);
        }

        UserViewModel ConvertToVM(User model)
        {
            UserViewModel vm = new UserViewModel(model);
            vm.EditRequested += Edit;
            vm.DeleteRequested += Delete;
            return vm;
        }

        public async void LoadUsers()
        {
            if (IsLoading)
            {
                return;
            }

            IsLoading = true;

            while (m_authVM.IsAuthenticated && !m_isDisposed)
            {
                try
                {
                    User[] users = await m_userRepository.SearchAsync(null, 0);
                    Users = users
                        .Select(x => ConvertToVM(x))
                        .ToArray();
                        
                    break;
                }
                catch (ApiException ex)
                {
                    await m_alertService.ShowAsync(
                        "Unable to load: " + ex.Message);
                        break;
                }
                catch (Exception ex)
                {
                    // Retry.
                    Console.WriteLine("Unable to load users:");
                    Console.WriteLine(ex.ToString());
                }
            }
            
            IsLoading = false;
        }

        async void Create()
        {
            User result = await m_editorService.ShowAsync(null);

            if (result != null)
            {
                LoadUsers();
            }
        }

        async void Edit(UserViewModel vm)
        {
            if (vm == null)
            {
                return;
            }

            User result = await m_editorService.ShowAsync(vm.Model);

            if (result != null)
            {
                LoadUsers();
            }
        }

        async void Delete(UserViewModel vm)
        {
            if (vm == null || IsLoading)
            {
                return;
            }

            // Prompting.
            bool canContinue = await m_alertService.PromptAsync(string.Format(
                "Yakin ingin menghapus user '{0}' ?",
                vm.Email));

            if (!canContinue)
            {
                return;
            }

            // Deleting.
            IsLoading = true;

            try
            {
                await m_userRepository.DeleteAsync(vm.Model.Id);
            }
            catch (Exception ex)
            {
                await m_alertService.ShowAsync("Unable to delete: " + ex.Message);
            }
            finally
            {
                IsLoading = false;
            }

            // Reload.
            LoadUsers();
        }

        public void Dispose()
        {
            m_isDisposed = true;
        }
    }
}