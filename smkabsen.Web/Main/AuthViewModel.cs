using System;
using System.ComponentModel;
using System.Threading.Tasks;
using smkabsen.Web.Services;
using smkabsen.Web.Models;
using smkabsen.Web.Repositories;

namespace smkabsen.Web.Main
{
    public class AuthViewModel : ViewModelBase
    {
        public delegate void AuthenticatedEventHandler();
        public event AuthenticatedEventHandler Authenticated;

        public delegate void DeauthenticatedEventHandler();
        public event DeauthenticatedEventHandler Deauthenticated;

        IUserInfoRepository m_userInfoRepository;

        bool m_isLoading;
        bool m_isAuthenticated;
        User m_user;

        public bool IsLoading
        {
            get
            {
                return m_isLoading;
            }
            set
            {
                m_isLoading = value;
                NotifyChange(nameof(IsLoading));
            }
        }

        public bool IsAuthenticated 
        {
            get
            {
                return m_isAuthenticated;
            }
            set
            {
                m_isAuthenticated = value;
                NotifyChange(nameof(IsAuthenticated));

                if (value)
                {
                    Authenticated?.Invoke();
                }
                else
                {
                    Deauthenticated?.Invoke();
                }
            }
        }

        public User User 
        {
            get
            {
                return m_user;
            }
            set
            {
                m_user = value;
                NotifyChange(nameof(User));
            }
        }

        public AuthViewModel(IUserInfoRepository userInfoRepository)
        {
            m_userInfoRepository = userInfoRepository;
            StartCheckingLoginStatus();
        }

        async void StartCheckingLoginStatus()
        {
            IsLoading = true;

            while (true)
            {
                try
                {
                    // Getting user info.
                    User = await m_userInfoRepository.GetAsync();
                    IsAuthenticated = true;

                    // Done.
                    break;
                }
                catch (ApiException)
                {
                    // No can do.
                    IsAuthenticated = false;
                    break;
                }
                catch (Exception ex)
                {
                    // Retry.
                    Console.WriteLine("Unable to check login:");
                    Console.WriteLine(ex.ToString());
                }
            }

            IsLoading = false;
        }
    }
}