using System;
using System.Net.Http;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Text;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using smkabsen.Web.Main;
using smkabsen.Web.Repositories;
using smkabsen.Web.Services;

namespace smkabsen.Web
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            WebAssemblyHostBuilder builder = WebAssemblyHostBuilder.CreateDefault(args);

            // Initializing app component.
            builder.RootComponents.Add<App>("app");

            // Configuration service.
            ConfigurationService configService = new ConfigurationService();
            configService.IsDevelopment = builder.HostEnvironment.IsDevelopment();

            // Injections.
            builder.Services

            // Repositories.
            .AddSingleton<IUserInfoRepository, UserInfoRepository>()
            .AddSingleton<ISchoolRepository, SchoolRepository>()
            .AddSingleton<IUserRepository, UserRepository>()

            // Services.
            .AddSingleton<IConfigurationService>(configService)
            .AddSingleton<ICookieService, CookieService>()
            .AddSingleton<IAlertService, AlertService>()
            .AddSingleton<IApiService, ApiService>()
            .AddSingleton<IAuthService, AuthService>()

            // Shared VMs.
            .AddSingleton<AuthViewModel>()

            // HTTP client.
            .AddSingleton(
                sp => new HttpClient 
                { 
                    BaseAddress = new Uri(builder.HostEnvironment.BaseAddress) 
                });

            // Run app.
            await builder.Build().RunAsync();
        }
    }
}
