window.methods = {
    createCookie: function(name, value, secs) {
        var d = new Date();
        d.setTime(d.getTime() + (secs * 1000));
        var expires = "expires="+ d.toUTCString();
        document.cookie = name + "=" + value + ";" + expires + ";path=/";
    },
    
    getCookie: function(name) {
        var cname = name + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for(var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while(c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if(c.indexOf(cname) == 0) {
                return c.substring(cname.length, c.length);
            }
        }
        return "";
    },

    showModal: function(id) {
        $("#"+id).modal(); // https://getbootstrap.com/docs/4.0/components/modal/#via-javascript
    },

    closeModal: function(id) {
        $("#"+id).modal("hide");
    },

    isModalOpen: function(id) {
        try {
            return $("#"+id).data('bs.modal')._isShown;
        } catch {
            return false;
        }
    },

    applyTypeahead: function(id) {

    }
}